<?php
/*
Plugin Name:        Caballero columns
Description:        WordPress column adaptation for bootstrap
Version:            1.1.6
Author:             Caballero
Author URI:         https://www.caballero.lv/
*/

namespace Caballero\Columns;

$caballero_columns = new Caballero_Columns();

class Caballero_Columns {


	private static $plugins      = array(
		'jsplus_bootstrap_include',
		'jsplus_bootstrap_show_blocks',
		// 'jsplus_bootstrap_block_conf',
		'jsplus_bootstrap_templates',
		// 'jsplus_bootstrap_row_add_up',
		// 'jsplus_bootstrap_row_add_down',
		// 'jsplus_bootstrap_button',
		// 'jsplus_bootstrap_icons',
		// 'jsplus_bootstrap_gallery',
		// 'jsplus_bootstrap_badge',
		// 'jsplus_bootstrap_label',
		// 'jsplus_bootstrap_breadcrumbs',
		// 'jsplus_bootstrap_alert',
		// 'jsplus_bootstrap_col_move_left',
		// 'jsplus_bootstrap_col_move_right',
		// 'jsplus_bootstrap_row_move_up',
		// 'jsplus_bootstrap_row_move_down',
		'jsplus_bootstrap_delete_col',
		'jsplus_bootstrap_delete_row',
	);
	private static $button_names = array(
		'jsplus_bootstrap_templates',
		// 'jsplus_bootstrap_row_add_up',
		// 'jsplus_bootstrap_row_add_down',
		// 'jsplus_bootstrap_col_move_left',
		// 'jsplus_bootstrap_col_move_right',
		// 'jsplus_bootstrap_row_move_up',
		// 'jsplus_bootstrap_row_move_down',
		'jsplus_bootstrap_delete_col',
		'jsplus_bootstrap_delete_row',
		'jsplus_bootstrap_show_blocks',
		// 'jsplus_bootstrap_button',
		// 'jsplus_bootstrap_icons',
		// 'jsplus_bootstrap_gallery',
		// 'jsplus_bootstrap_badge',
		// 'jsplus_bootstrap_label',
		// 'jsplus_bootstrap_breadcrumbs',
		// 'jsplus_bootstrap_alert',
		// 'jsplus_bootstrap_block_conf'
	);

	public function __construct() {
		/**
		 * Add the TinyMCE Plugins and buttons.
		 */
		if ( is_admin() ) {

			add_action( 'mce_css', array( $this, 'caballero_add_editor_styles' ) );
			add_filter( 'tiny_mce_before_init', array( $this, 'caballero_filter_tiny_mce_before_init' ) );
			add_filter( 'mce_external_plugins', array( $this, 'caballero_columns_plugins' ) );
			add_filter( 'mce_buttons_3', array( $this, 'caballero_register_tinymce_button' ) );
		}

	}

	public function caballero_add_editor_styles( $mce_css ) {

		if ( ! empty( $mce_css ) ) {
			$mce_css .= ',';
		}

		$mce_css .= plugins_url( 'css/', __FILE__ ) . 'flexbox.min.css,';
		// $mce_css .= plugins_url('css/', __FILE__).'bootstrap.min.css,';
		// $mce_css .= plugins_url('css/', __FILE__).'bootstrap-theme.min.css,';
		// $mce_css .= plugins_url('css/', __FILE__).'bootstrap-styles.css';

		return $mce_css;
	}

	function caballero_filter_tiny_mce_before_init( $options ) {

		if ( ! isset( $options['extended_valid_elements'] ) ) {
			$options['extended_valid_elements'] = '';
		} else {
			$options['extended_valid_elements'] .= ',';
		}

		$options['extended_valid_elements'] .= 'div[*],span[*],i[*]';
		return $options;
	}

	function caballero_columns_plugins( $plugin_array ) {
		// $plugin_array['bootstrap_js'] = plugins_url('js/', __FILE__) . 'bootstrap.min.js';

		foreach ( self::$plugins as $plugin ) {
			$plugin_array[ $plugin ] = plugins_url( 'tinymce/', __FILE__ ) . $plugin . '/editor_plugin.js';
		}

		return $plugin_array;
	}

	function caballero_register_tinymce_button( $buttons ) {
		foreach ( self::$button_names as $btn ) {
			array_push( $buttons, $btn );
		}

		return $buttons;
	}

}
