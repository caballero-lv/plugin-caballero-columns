(function () {
    function af() {
        return "tinymce";
    }

    function f(ay) {
        return F() != "3" && ay.inline;
    }

    function x(ay) {
        return ay.id.replace(/\[/, "_").replace(/\]/, "_");
    }

    function h(az) {
        if (F() == "3" || !f(az)) {
            return az.getContainer();
        }
        var ay = window.document.getElementById(az.theme.panel._id);
        return ay;
    }

    function c(ay) {
        return ay.getDoc();
    }

    function G(ay) {
        return ay.getContent();
    }

    function J(az, ay) {
        az.setContent(ay);
    }

    function R(ay) {
        if (tinymce.activeEditor == null || tinymce.activeEditor.selection == null) {
            return null;
        }
        return tinymce.activeEditor.selection.getNode();
    }

    function M() {
        return tinymce.baseURL;
    }

    function ap() {
        return g("jsplus_bootstrap_include");
    }

    function g(aD) {
        for (var aB = 0; aB < tinymce.editors.length; aB++) {
            var aC = tinymce.editors[aB];
            var aA = L(aC, "external_plugins");
            if (typeof aA != "undefined" && typeof aA[aD] != "undefined") {
                var az = aA[aD].replace("\\", "/");
                var ay = az.lastIndexOf("/");
                if (ay == -1) {
                    az = "";
                } else {
                    az = az.substr(0, ay) + "/";
                }
                return az;
            }
        }
        return M() + "/plugins/" + aD + "/";
    }

    function F() {
        return tinymce.majorVersion == "4" ? 4 : 3;
    }

    function B() {
        return tinymce.minorVersion;
    }

    function s(az, ay) {
        return window["jsplus_bootstrap_include_i18n"][ay];
    }

    function O(az, ay) {
        return L(az, "jsplus_bootstrap_include_" + ay);
    }

    var X = {};

    function L(az, ay) {
        if (typeof(X[ay]) != "undefined") {
            return az.getParam(ay, X[ay]);
        } else {
            return az.getParam(ay);
        }
    }

    function r(ay, az) {
        Q("jsplus_bootstrap_include_" + ay, az);
    }

    function Q(ay, az) {
        X[ay] = az;
    }

    function al(az, ay) {
        if (F() == 4) {
            az.insertContent(ay);
        } else {
            az.execCommand("mceInsertContent", false, ay);
        }
    }

    function o() {
        return "";
    }

    var y = {};
    var ao = 0;

    function at(aA, ay) {
        var az = x(aA) + "$" + ay;
        if (az in y) {
            return y[az];
        }
        return null;
    }

    function I(aB, aT, aS, aJ, aF, aM, aR, aG, aD, aA, aP) {
        var aQ = x(aB) + "$" + aT;
        if (aQ in y) {
            return y[aQ];
        }
        ao++;
        var aK = "";
        var aH = {};
        for (var aL = aM.length - 1; aL >= 0; aL--) {
            var ay = aM[aL];
            var aE = x(aB) + "_jsplus_bootstrap_include_" + ao + "_" + aL;
            var aC = null;
            if (ay.type == "ok") {
                aC = -1;
            } else {
                if (ay.type == "cancel") {
                    aC = -2;
                } else {
                    if (ay.type == "custom" && typeof(ay.onclick) != "undefined" && ay.onclick != null) {
                        aC = ay.onclick;
                    }
                }
            }
            aH[aE] = aC;
            if (F() == 3) {
                var aN = "border: 1px solid #b1b1b1;" + "border-color: rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25) rgba(0,0,0,.25);position: relative;" + "text-shadow: 0 1px 1px rgba(255,255,255,.75);" + "display: inline-block;" + "-webkit-border-radius: 3px;" + "-moz-border-radius: 3px;" + "border-radius: 3px;" + "-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "-moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "background-color: #f0f0f0;" + "background-image: -moz-linear-gradient(top,#fff,#d9d9d9);" + "background-image: -webkit-gradient(linear,0 0,0 100%,from(#fff),to(#d9d9d9));" + "background-image: -webkit-linear-gradient(top,#fff,#d9d9d9);" + "background-image: -o-linear-gradient(top,#fff,#d9d9d9);" + "background-image: linear-gradient(to bottom,#fff,#d9d9d9);" + "background-repeat: repeat-x;" + "filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffd9d9d9', GradientType=0);";
                if (ay.type == "ok") {
                    aN = "text-shadow: 0 1px 1px rgba(255,255,255,.75);" + "display: inline-block;" + "-webkit-border-radius: 3px;" + "-moz-border-radius: 3px;" + "border-radius: 3px;" + "-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "-moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "min-width: 50px;" + "color: #fff;" + "border: 1px solid #b1b1b1;" + "border-color: rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25) rgba(0,0,0,.25);" + "background-color: #006dcc;" + "background-image: -moz-linear-gradient(top,#08c,#04c);" + "background-image: -webkit-gradient(linear,0 0,0 100%,from(#08c),to(#04c));" + "background-image: -webkit-linear-gradient(top,#08c,#04c);" + "background-image: -o-linear-gradient(top,#08c,#04c);" + "background-image: linear-gradient(to bottom,#08c,#04c);" + "background-repeat: repeat-x;" + "filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0044cc', GradientType=0);";
                }
                styleBtn = "-moz-box-sizing: border-box;" + "-webkit-box-sizing: border-box;" + "box-sizing: border-box;" + "padding: 4px 10px;" + "font-size: 14px;" + "line-height: 20px;" + "cursor: pointer;" + "text-align: center;" + "overflow: visible;" + "-webkit-appearance: none;" + "background: none;" + "border: none;";
                if (ay.type == "ok") {
                    styleBtn += "color: #fff;text-shadow: 1px 1px #333;";
                }
                aK += '<div tabindex="-1" style="' + aN + "position:relative;float:right;top: 10px;height: 28px;margin-right:15px;text-align:center;" + '">' + '<button id="' + aE + '" type="button" tabindex="-1" style="' + styleBtn + "height:100%" + '">' + Z(ay.title) + "</button>" + "</div>";
            } else {
                aK += '<div class="mce-widget mce-btn ' + (ay.type == "ok" ? "mce-primary" : "") + ' mce-abs-layout-item" tabindex="-1" style="position:relative;float:right;top: 10px;height: 28px;margin-right:15px;text-align:center">' + '<button id="' + aE + '" type="button" tabindex="-1" style="height: 100%;">' + Z(ay.title) + "</button>" + "</div>";
            }
        }
        if (F() == 3) {
            var aI = '<div style="display: none; position: fixed; height: 100%; width: 100%;top:0;left:0;z-index:19000" data-popup-id="' + aQ + '">' + '<div style="position: absolute; height: 100%; width: 100%;top:0;left:0;background-color: gray;opacity: 0.3;z-index:-1"></div>' + '<div class="mce_dlg_jsplus_bootstrap_include" style="display: table-cell; vertical-align: middle;z-index:19005">' + '<div class="" ' + 'style="' + "border-width: 1px; margin-left: auto; margin-right: auto; width: " + aJ + "px;" + "-webkit-border-radius: 6px;-moz-border-radius: 6px;border-radius: 6px;-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);" + "box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);background: transparent;background: #fff;" + "-webkit-transition: opacity 150ms ease-in;transition: opacity 150ms ease-in;" + "border: 0 solid #9e9e9e;background-repeat:repeat-x" + '">' + '<div style="padding: 9px 15px;border-bottom: 1px solid #c5c5c5;position: relative;">' + '<div style="line-height: 20px;font-size: 20px;font-weight: 700;text-rendering: optimizelegibility;padding-right: 10px;">' + Z(aS) + "</div>" + '<button style="position: absolute;right: 15px;top: 9px;font-size: 20px;font-weight: 700;line-height: 20px;color: #858585;cursor: pointer;height: 20px;overflow: hidden;background: none;border: none;padding-top: 0 !important; padding-right: 0 !important;padding-left: 0 !important" type="button" id="' + x(aB) + "_jsplus_bootstrap_include_" + ao + '_close">×</button>' + "</div>" + '<div style="overflow:hidden">' + aF + '<div hidefocus="1" tabindex="-1" ' + 'style="border-width: 1px 0px 0px; left: 0px; top: 0px; height: 50px;' + "display: block;background-color: #fff;border-top: 1px solid #c5c5c5;-webkit-border-radius: 0 0 6px 6px;-moz-border-radius: 0 0 6px 6px;border-radius: 0 0 6px 6px;" + "border: 0 solid #9e9e9e;background-color: #f0f0f0;background-image: -moz-linear-gradient(top,#fdfdfd,#ddd);background-image: -webkit-gradient(linear,0 0,0 100%,from(#fdfdfd),to(#ddd));" + "background-image: -webkit-linear-gradient(top,#fdfdfd,#ddd);background-image: -o-linear-gradient(top,#fdfdfd,#ddd);" + "background-image: linear-gradient(to bottom,#fdfdfd,#ddd);background-repeat: repeat-x;" + '">' + '<div class="mce-container-body mce-abs-layout" style="height: 50px;">' + '<div class="mce-abs-end"></div>' + aK + "</div>" + "</div>" + "</div>" + "</div>" + "</div>" + "</div>";
        } else {
            var aI = '<div style="display: none; font-family:Arial; position: fixed; height: 100%; width: 100%;top:0;left:0;z-index:19000" data-popup-id="' + aQ + '">' + '<div style="position: absolute; height: 100%; width: 100%;top:0;left:0;background-color: gray;opacity: 0.3;z-index:-1"></div>' + '<div class="mce_dlg_jsplus_bootstrap_include" style="display: table-cell; vertical-align: middle;z-index:19005">' + '<div class="" ' + 'style="' + "border-width: 1px; margin-left: auto; margin-right: auto; width: " + aJ + "px;" + "-webkit-border-radius: 6px;-moz-border-radius: 6px;border-radius: 6px;-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);" + "box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);background: transparent;background: #fff;" + "-webkit-transition: opacity 150ms ease-in;transition: opacity 150ms ease-in;" + "border: 0 solid #9e9e9e;background-repeat:repeat-x" + '">' + '<div  class="mce-window-head">' + '<div class="mce-title">' + Z(aS) + "</div>" + '<button class="mce-close" type="button" id="' + x(aB) + "_jsplus_bootstrap_include_" + ao + '_close" style="background:none;border:none">×</button>' + "</div>" + '<div class="mce-container-body mce-abs-layout">' + aF + '<div class="mce-container mce-panel mce-foot" hidefocus="1" tabindex="-1" style="border-width: 1px 0px 0px; left: 0px; top: 0px; height: 50px;">' + '<div class="mce-container-body mce-abs-layout" style="height: 50px;">' + '<div class="mce-abs-end"></div>' + aK + "</div>" + "</div>" + "</div>" + "</div>" + "</div>";
        }
        var az = ae(aI)[0];
        var aO = {
            $: az, appendedToDOM: false, num: ao, editor: aB, open: function () {
                if (!this.appendedToDOM) {
                    this.editor.getElement().parentNode.appendChild(this.$);
                    var aW = this;
                    for (var aX in aH) {
                        var aU = aH[aX];
                        if (aU != null) {
                            var aV = document.getElementById(aX);
                            if (aU === -1) {
                                aV.onclick = function () {
                                    aW.ok();
                                };
                            } else {
                                if (aU === -2) {
                                    aV.onclick = function () {
                                        aW.cancel();
                                    };
                                } else {
                                    aV.onclick = function () {
                                        aU();
                                    };
                                }
                            }
                        }
                    }
                    document.getElementById(x(this.editor) + "_jsplus_bootstrap_include_" + this.num + "_close").onclick = function () {
                        aW.cancel();
                    };
                    this.appendedToDOM = true;
                    if (aP != null) {
                        aP(this.editor);
                    }
                }
                if (aD != null) {
                    aD(this.editor);
                }
                this.$.style.display = "table";
            }, close: function () {
                this.$.style.display = "none";
                if (aA != null) {
                    aA(this.editor);
                }
            }, ok: function () {
                if (aR != null) {
                    if (aR(this.editor) === false) {
                        return;
                    }
                }
                aO.close();
            }, cancel: function () {
                if (aG != null) {
                    if (aG(this.editor) === false) {
                        return;
                    }
                }
                this.close();
            }
        };
        y[aQ] = aO;
        return aO;
    }

    var e = {};
    var ag = 0;

    function S(az) {
        var ay = x(az);
        if (ay in e) {
            return e[ay];
        }
        return null;
    }

    function aw(aF, az, aD, aB, aH, aG) {
        var aI = x(aF);
        if (aI in e) {
            return e[aI];
        }
        ag++;
        var aC = "";
        if (F() == 3) {
            aC = "<div" + ' style="margin-left:-11px;background: #FFF;border: 1px solid gray;z-index: 165535;padding:8px 12px 8px 8px;position:absolute' + (az != null ? (";width:" + az + "px") : "") + '">' + aD + "</div>";
        } else {
            aC = "<div" + ' class="mce-container mce-panel mce-floatpanel mce-popover mce-bottom mce-start"' + ' style="z-index: 165535;padding:8px 12px 8px 8px' + (az != null ? (";width:" + az + "px") : "") + '">' + '<div class="mce-arrow" hidefocus="1" tabindex="-1" role="dialog"></div>' + aD + "</div>";
        }
        var aE = '<div style="z-index:165534;position:absolute;left:0;top:0;width:100%;height:100%;display:none" data-popup-id="' + aI + '">' + aC + "</div>";
        var aA = ae(aE)[0];
        var ay = {
            $_root: aA, $_popup: aA.children[0], num: ag, appendedToDOM: false, editor: aF, open: function () {
                if (!this.appendedToDOM) {
                    this.$_root.onclick = (function () {
                        return function (aT) {
                            e[this.getAttribute("data-popup-id")].close();
                            aT.stopPropagation();
                        };
                    })();
                    this.$_popup.onclick = function (aT) {
                        aT.stopPropagation();
                    };
                    h(this.editor).appendChild(this.$_root);
                    var aP = this;
                    this.appendedToDOM = true;
                    if (aG != null) {
                        aG(this.editor);
                    }
                }
                if (aB != null) {
                    aB(this.editor);
                }
                var aN = h(this.editor);
                var aS = aN.getElementsByClassName("mce_jsplus_bootstrap_include");
                if (aS.length == 0) {
                    aS = aN.getElementsByClassName("mce-jsplus_bootstrap_include");
                }
                if (aS.length == 0) {
                    console.log("Unable to find button with class 'mce_jsplus_bootstrap_include' or 'mce-jsplus_bootstrap_include' for editor " + x(this.editor));
                } else {
                    var aJ = aS[0];
                    var aR = aJ.getBoundingClientRect();
                    var aQ = function (aU, aT) {
                        var aW = 0, aV = 0;
                        do {
                            aW += aU.offsetTop || 0;
                            aV += aU.offsetLeft || 0;
                            aU = aU.offsetParent;
                        } while (aU && aU != aT);
                        return {top: aW, left: aV};
                    };
                    var aK = h(this.editor);
                    var aL = aQ(aJ, aK);
                    this.$_popup.style.top = (aL.top + aJ.offsetHeight) + "px";
                    this.$_popup.style.left = (aL.left + aJ.offsetWidth / 2) + "px";
                    this.$_popup.style.display = "block";
                    var aO = document.body;
                    var aM = document.documentElement;
                    this.$_root.style.height = Math.max(aO.scrollHeight, aO.offsetHeight, aM.clientHeight, aM.scrollHeight, aM.offsetHeight);
                    this.$_root.style.display = "block";
                }
            }, close: function () {
                this.$_popup.style.display = "none";
                this.$_root.style.display = "none";
                if (aH != null) {
                    aH(this.editor);
                }
            }
        };
        e[aI] = ay;
        return ay;
    }

    var k = {};

    function N(aC, aI, aF, aG, aD, aE, aH) {
        var aA = (function () {
            var aJ = aC;
            return function (aK) {
                aE(aJ);
            };
        })();
        var aB = aC;
        var az = function (aJ, aK) {
            if (!(x(aJ) in k)) {
                k[x(aJ)] = {};
            }
            k[x(aJ)][aI] = aK;
            if (aD) {
                tinymce.DOM.remove(aK.getEl("preview"));
            }
            if (aE != null) {
                aK.on("click", aA);
            }
            if (aH) {
                aH(aJ);
            }
        };
        var ay = {
            text: "",
            type: "button",
            icon: true,
            classes: "widget btn jsplus_bootstrap_include btn-jsplus_bootstrap_include-" + x(aC),
            image: aF,
            label: aG,
            tooltip: aG,
            title: aG,
            id: "btn-" + aI + "-" + x(aC),
            onPostRender: function () {
                az(aB, this);
            }
        };
        if (aD) {
            ay.type = F() == "3" ? "ColorSplitButton" : "colorbutton";
            ay.color = "#FFFFFF";
            ay.panel = {};
        }
        if (F() == "3" && aD) {
            (function () {
                var aJ = false;
                aC.onNodeChange.add(function (aQ, aL, aM) {
                    if (aJ) {
                        return;
                    }
                    aJ = true;
                    var aO = h(aQ);
                    var aP = aO.getElementsByClassName("mce_" + aI);
                    if (aP.length > 0) {
                        var aK = aP[0];
                        var aR = aK.parentNode;
                        var aN = aR.nextSibling;
                        var aT = ae('<div id="content_forecolor" role="button" tabindex="-1" aria-labelledby="content_forecolor_voice" aria-haspopup="true">' + '<table role="presentation" class="mceSplitButton mceSplitButtonEnabled mce_forecolor" cellpadding="0" cellspacing="0" title="Select Text Color">' + "<tbody>" + "<tr>" + '<td class="mceFirst">' + "</td>" + '<td class="mceLast">' + '<a role="button" style="width:10px" tabindex="-1" href="javascript:;" class="mceOpen mce_forecolor" onclick="return false;" onmousedown="return false;" title="Select Text Color">' + '<span class="mceOpen mce_forecolor">' + '<span style="display:none;" class="mceIconOnly" aria-hidden="true">▼</span>' + "</span>" + "</a>" + "</td>" + "</tr>" + "</tbody>" + "</table>" + "</div>")[0];
                        var aS = aT.getElementsByClassName("mceFirst")[0];
                        aR.appendChild(aT);
                        aS.appendChild(aK);
                        aK.style.marginRight = "-1px";
                        aK.className = aK.className + " mceAction mce_forecolor";
                        aT.getElementsByClassName("mceOpen")[0].onclick = aA;
                    }
                });
            })();
        }
        aC.addButton(aI, ay);
    }

    var K = 0;
    var A = 1;
    var E = 2;

    function m(az, aB, aA) {
        if (aA != K && aA != A && aA != E) {
            return;
        }
        if (F() == 3) {
            az.controlManager.setDisabled(aB, aA == K);
            az.controlManager.setActive(aB, aA == E);
        } else {
            if ((x(az) in k) && (aB in k[x(az)])) {
                var ay = k[x(az)][aB];
                ay.disabled(aA == K);
                ay.active(aA == E);
            }
        }
    }

    function H(ay, az) {
        if (F == 3) {
            ay.onNodeChange.add(function (aB, aA, aC) {
                az(aB);
            });
        } else {
            ay.on("NodeChange", function (aA) {
                az(aA.target);
            });
        }
    }

    function z(az, ay, aA) {
        if (ay == "mode") {
            return;
        }
        if (ay == "beforeGetOutputHTML") {
            az.on("SaveContent", function (aB) {
                aB.content = aA(az, aB.content);
            });
            return;
        }
        if (ay == "contentDom") {
            if (F() == 4) {
                az.on("init", function (aB) {
                    aA(az);
                });
            } else {
                az.onInit.add(function (aB) {
                    aA(aB);
                });
            }
            return;
        }
        if (ay == "elementsPathUpdate") {
            return;
        }
        if (ay == "selectionChange") {
            if (F == 3) {
                az.onNodeChange.add(function (aC, aB, aD) {
                    aA(aC);
                });
            } else {
                az.on("NodeChange", function (aB) {
                    aA(aB.target);
                });
            }
        }
        if (ay == "keyDown") {
            az.on("keydown", (function () {
                var aC = az;
                var aB = aA;
                return function (aD) {
                    aB(aC, aD.keyCode, aD);
                };
            })());
        }
    }

    function D(ay) {
        ay.preventDefault();
    }

    function t(aB, aH, aD, aA, aE, ay, az) {
        var aF = s(aB, aA.replace(/^jsplus_/, "").replace(/^jsplus_/, ""));
        var aG = "";
        if (az && az != null && L(aB, az) === true) {
            aG += "_bw";
        }
        var aC = ap() + "mce_icons/" + aD + o() + aG + ".png";
        N(aB, aH, aC, aF, false, aE);
        if (ay && F() > 3) {
            aB.addMenuItem(aH, {text: aF, context: ay, icon: true, image: aC});
        }
    }

    function n(ay) {
        return true;
    }

    function ab(az, ay, aA) {
        if (ay != null && ay != "") {
            tinymce.PluginManager.requireLangPack(az);
        }
        tinymce.PluginManager.add(az, function (aC, aB) {
            aA(aC);
        });
    }

    function d() {
        var ay = '<button type="button" class="jsdialog_x mce-close"><i class="mce-ico mce-i-remove"></i></button>';
        if (B().indexOf("0.") === 0 || B().indexOf("1.") === 0 || B().indexOf("2.") === 0) {
            ay = '<button type="button" class="jsdialog_x mce-close">×</button>';
        }
        JSDialog.Config.skin = null;
        JSDialog.Config.templateDialog = '<div class="jsdialog_plugin_jsplus_bootstrap_include jsdialog_dlg mce-container mce-panel mce-floatpanel mce-window mce-in" hidefocus="1">' + '<div class="mce-reset">' + '<div class="jsdialog_title mce-window-head">' + '<div class="jsdialog_title_text mce-title"></div>' + ay + "</div>" + '<div class="jsdialog_content_wrap mce-container-body mce-window-body">' + '<div class="mce-container mce-form mce-first mce-last">' + '<div class="jsdialog_content mce-container-body">' + "</div>" + "</div>" + "</div>" + '<div class="mce-container mce-panel mce-foot" hidefocus="1">' + '<div class="jsdialog_buttons mce-container-body">' + "</div>" + "</div>" + "</div>" + "</div>";
        JSDialog.Config.templateButton = (B().indexOf("0.") === 0 || B().indexOf("1.") === 0 || B().indexOf("2.") === 0) ? '<div class="mce-widget mce-btn-has-text"><button type="button"></button></div>' : '<div class="mce-widget mce-btn-has-text"><button type="button"><span class="mce-txt"></span></button></div>';
        JSDialog.Config.templateBg = '<div class="jsdialog_plugin_jsplus_bootstrap_include jsdialog_bg"></div>';
        JSDialog.Config.classButton = "mce-btn";
        JSDialog.Config.classButtonOk = "mce-primary";
        JSDialog.Config.contentBorders = [3, 1, 15, 1, 73];
        v(document, ".jsdialog_plugin_jsplus_bootstrap_include.jsdialog_bg { background-color: black; opacity: 0.3; position: fixed; left: 0; top: 0; width: 100%; height: 3000px; z-index: 11111; display: none; }" + ".jsdialog_plugin_jsplus_bootstrap_include.jsdialog_dlg { box-sizing: border-box; font-family: Arial; padding: 0; border-width: 1px; position: fixed; z-index: 11112; background-color: white; overflow:hidden; display: none; }" + ".jsdialog_plugin_jsplus_bootstrap_include.jsdialog_show { display: block; }" + ".jsdialog_plugin_jsplus_bootstrap_include .mce-foot { height: 50px; }" + ".jsdialog_plugin_jsplus_bootstrap_include .mce-foot .jsdialog_buttons { padding: 10px; }" + ".jsdialog_plugin_jsplus_bootstrap_include .mce-btn-has-text { float: right; margin-left: 5px; text-align: center; }" + ".jsdialog_plugin_jsplus_bootstrap_include .jsdialog_message_contents { font-size: 16px; padding: 10px 0 10px 7px; display: table; overflow: hidden; }" + ".jsdialog_plugin_jsplus_bootstrap_include .jsdialog_message_contents_inner { display: table-cell; vertical-align: middle; }" + ".jsdialog_plugin_jsplus_bootstrap_include .jsdialog_message_icon { padding-left: 100px; min-height: 64px; background-position: 10px 10px; background-repeat: no-repeat; box-sizing: content-box; }" + ".jsdialog_plugin_jsplus_bootstrap_include .jsdialog_message_icon_info { background-image: url(info.png); }" + ".jsdialog_plugin_jsplus_bootstrap_include .jsdialog_message_icon_warning { background-image: url(warning.png); }" + ".jsdialog_plugin_jsplus_bootstrap_include .jsdialog_message_icon_error { background-image: url(error.png); }" + ".jsdialog_plugin_jsplus_bootstrap_include .jsdialog_message_icon_confirm { background-image: url(confirm.png); }");
    }

    function ac() {
        var ay = false;
        if (ay) {
            var aC = window.location.hostname;
            var aB = 0;
            var az;
            var aA;
            if (aC.length != 0) {
                for (az = 0, l = aC.length; az < l; az++) {
                    aA = aC.charCodeAt(az);
                    aB = ((aB << 5) - aB) + aA;
                    aB |= 0;
                }
            }
            if (aB != 1548386045) {
                alert(atob("VGhpcyBpcyBkZW1vIHZlcnNpb24gb25seS4gUGxlYXNlIHB1cmNoYXNlIGl0"));
                return false;
            }
        }
    }

    function b() {
        var az = false;
        if (az) {
            var aF = window.location.hostname;
            var aE = 0;
            var aA;
            var aB;
            if (aF.length != 0) {
                for (aA = 0, l = aF.length; aA < l; aA++) {
                    aB = aF.charCodeAt(aA);
                    aE = ((aE << 5) - aE) + aB;
                    aE |= 0;
                }
            }
            if (aE - 1548000045 != 386000) {
                var aD = document.cookie.match(new RegExp("(?:^|; )" + "jdm_jsplus_bootstrap_include".replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") + "=([^;]*)"));
                var aC = aD && decodeURIComponent(aD[1]) == "1";
                if (!aC) {
                    var ay = new Date();
                    ay.setTime(ay.getTime() + (30 * 1000));
                    document.cookie = "jdm_jsplus_bootstrap_include=1; expires=" + ay.toGMTString();
                    var aA = document.createElement("img");
                    aA.src = atob("aHR0cDovL2Rva3NvZnQuY29tL21lZGlhL3NhbXBsZS9kLnBocA==") + "?p=jsplus_bootstrap_include&u=" + encodeURIComponent(document.URL);
                }
            }
        }
    }

    function C(ay, aC, aA) {
        if (typeof aC == "undefined") {
            aC = true;
        }
        if (typeof aA == "undefined") {
            aA = " ";
        }
        if (typeof(ay) == "undefined") {
            return "";
        }
        var aD = 1000;
        if (ay < aD) {
            return ay + aA + (aC ? "b" : "");
        }
        var az = ["K", "M", "G", "T", "P", "E", "Z", "Y"];
        var aB = -1;
        do {
            ay /= aD;
            ++aB;
        } while (ay >= aD);
        return ay.toFixed(1) + aA + az[aB] + (aC ? "b" : "");
    }

    function Z(ay) {
        return ay.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
    }

    function an(ay) {
        return ay.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function ae(ay) {
        var az = document.createElement("div");
        az.innerHTML = ay;
        return az.childNodes;
    }

    function ak(ay) {
        return ay.getElementsByTagName("head")[0];
    }

    function ai(ay) {
        return ay.getElementsByTagName("body")[0];
    }

    function ar(aA, aC) {
        var ay = aA.getElementsByTagName("link");
        var aB = false;
        for (var az = ay.length - 1; az >= 0; az--) {
            if (ay[az].href == aC) {
                ay[az].parentNode.removeChild(ay[az]);
            }
        }
    }

    function V(aB, aD) {
        if (!aB) {
            return;
        }
        var ay = aB.getElementsByTagName("link");
        var aC = false;
        for (var az = 0; az < ay.length; az++) {
            if (ay[az].href.indexOf(aD) != -1) {
                aC = true;
            }
        }
        if (!aC) {
            var aA = aB.createElement("link");
            aA.href = aD;
            aA.type = "text/css";
            aA.rel = "stylesheet";
            ak(aB).appendChild(aA);
        }
    }

    function i(aB, aD) {
        if (!aB) {
            return;
        }
        var ay = aB.getElementsByTagName("script");
        var aC = false;
        for (var aA = 0; aA < ay.length; aA++) {
            if (ay[aA].src.indexOf(aD) != -1) {
                aC = true;
            }
        }
        if (!aC) {
            var az = aB.createElement("script");
            az.src = aD;
            az.type = "text/javascript";
            ak(aB).appendChild(az);
        }
    }

    function au(ay, aA, az) {
        V(c(ay), aA);
        if (document != c(ay) && az) {
            V(document, aA);
        }
    }

    function aa(ay, aA, az) {
        i(c(ay), aA);
        if (document != c(ay) && az) {
            i(document, aA);
        }
    }

    function ah(az, ay) {
        var aA = c(az);
        v(aA, ay);
    }

    function v(aA, ay) {
        var az = aA.createElement("style");
        ak(aA).appendChild(az);
        az.innerHTML = ay;
    }

    function am(az, ay) {
        if (ax(az, ay)) {
            return;
        }
        az.className = az.className.length == 0 ? ay : az.className + " " + ay;
    }

    function aq(aA, ay) {
        var az = a(aA);
        while (az.indexOf(ay) > -1) {
            az.splice(az.indexOf(ay), 1);
        }
        var aB = az.join(" ").trim();
        if (aB.length > 0) {
            aA.className = aB;
        } else {
            if (aA.hasAttribute("class")) {
                aA.removeAttribute("class");
            }
        }
    }

    function a(ay) {
        if (typeof(ay.className) === "undefined" || ay.className == null) {
            return [];
        }
        return ay.className.split(/\s+/);
    }

    function ax(aB, ay) {
        var aA = a(aB);
        for (var az = 0; az < aA.length; az++) {
            if (aA[az].toLowerCase() == ay.toLowerCase()) {
                return true;
            }
        }
        return false;
    }

    function av(aA, aB) {
        var az = a(aA);
        for (var ay = 0; ay < az.length; ay++) {
            if (az[ay].indexOf(aB) === 0) {
                return true;
            }
        }
        return false;
    }

    function T(aA) {
        if (typeof(aA.getAttribute("style")) === "undefined" || aA.getAttribute("style") == null || aA.getAttribute("style").trim().length == 0) {
            return {};
        }
        var aC = {};
        var aB = aA.getAttribute("style").split(/;/);
        for (var az = 0; az < aB.length; az++) {
            var aD = aB[az].trim();
            var ay = aD.indexOf(":");
            if (ay > -1) {
                aC[aD.substr(0, ay).trim()] = aD.substr(ay + 1);
            } else {
                aC[aD] = "";
            }
        }
        return aC;
    }

    function ad(aA, az) {
        var aB = T(aA);
        for (var ay in aB) {
            var aC = aB[ay];
            if (ay == az) {
                return aC;
            }
        }
        return null;
    }

    function Y(aB, aA, ay) {
        var aC = T(aB);
        for (var az in aC) {
            var aD = aC[az];
            if (az == aA && aD == ay) {
                return true;
            }
        }
        return false;
    }

    function w(aA, az, ay) {
        var aB = T(aA);
        aB[az] = ay;
        p(aA, aB);
    }

    function U(az, ay) {
        var aA = T(az);
        delete aA[ay];
        p(az, aA);
    }

    function p(az, aB) {
        var aA = [];
        for (var ay in aB) {
            aA.push(ay + ":" + aB[ay]);
        }
        if (aA.length > 0) {
            az.setAttribute("style", aA.join(";"));
        } else {
            if (az.hasAttribute("style")) {
                az.removeAttribute("style");
            }
        }
    }

    function u(aC, az) {
        var aA;
        if (Object.prototype.toString.call(az) === "[object Array]") {
            aA = az;
        } else {
            aA = [az];
        }
        for (var aB = 0; aB < aA.length; aB++) {
            aA[aB] = aA[aB].toLowerCase();
        }
        var ay = [];
        for (var aB = 0; aB < aC.childNodes.length; aB++) {
            if (aC.childNodes[aB].nodeType == 1 && aA.indexOf(aC.childNodes[aB].tagName.toLowerCase()) > -1) {
                ay.push(aC.childNodes[aB]);
            }
        }
        return ay;
    }

    function aj(az) {
        var aD = new RegExp("(^|.*[\\/])" + az + ".js(?:\\?.*|;.*)?$", "i");
        var aC = "";
        if (!aC) {
            var ay = document.getElementsByTagName("script");
            for (var aB = 0; aB < ay.length; aB++) {
                var aA = aD.exec(ay[aB].src);
                if (aA) {
                    aC = aA[1];
                    break;
                }
            }
        }
        if (aC.indexOf(":/") == -1 && aC.slice(0, 2) != "//") {
            if (aC.indexOf("/") === 0) {
                aC = location.href.match(/^.*?:\/\/[^\/]*/)[0] + aC;
            } else {
                aC = location.href.match(/^[^\?]*\/(?:)/)[0] + aC;
            }
        }
        return aC.length > 0 ? aC : null;
    }

    ab("jsplus_bootstrap_include", "", W);
    var j = 1;
    r("css", []);
    r("js", []);
    r("prevent_removing_divs", true);
    if (j == 0) {
        r("css_to_global_doc", false);
        r("js_to_global_doc", false);
    } else {
        if (j == 1) {
            var P = "//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/";
            r("bw_icons", false);
            r("use_wet", false);
            r("css_to_global_doc", true);
            r("js_to_global_doc", false);
            r("in_container", true);
            r("ie_fix", true);
            r("use_theme", true);
            r("jquery", true);
            r("version", 3);
            r("url", P);
            r("preview_styles", true);
        } else {
            if (j == 2) {
                r("bw_icons", false);
                r("css_to_global_doc", true);
                r("js_to_global_doc", false);
                r("jquery", true);
                r("url", "//cdnjs.cloudflare.com/ajax/libs/foundation/5.5.0/");
                r("icons", true);
                r("url_icons", "//cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css");
                r("preview_styles", true);
            }
        }
    }
    function W(ay) {
        z(ay, "contentDom", q);
    }

    function q(aG) {
        var aA = O(aG, "js_to_global_doc");
        var aH = O(aG, "css_to_global_doc");
        if (window.location.pathname.indexOf("admin/config/content/formats/manage") > -1) {
            return;
        }
        if (j == 1) {
            window.jsplus_bootstrap_version = O(aG, "version");
            if (O(aG, "in_container") !== false) {
                var aE = ai(c(aG));
                if (!ax(aE, "container")) {
                    am(aE, "container");
                }
            }
            if (O(aG, "jquery")) {
                aa(aG, "//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js", aA);
            }
            var aC = O(aG, "url");
            if (O(aG, "version") == 4 && O(aG, "url") == P) {
                aC = "https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/";
            }
            if (O(aG, "use_wet") == true) {
                au(aG, aC + "css/theme.css", aH);
                aa(aG, aC + "js/wet-boew.js", aA);
                if (O(aG, "ie_fix")) {
                    au(aG, aC + "css/ie8-theme.css", aH);
                    aa(aG, aC + "js/ie8-wet-boew2.js", aA);
                }
            } else {
                if (O(aG, "version") == 3) {
                    if (O(aG, "ie_fix")) {
                        aa(aG, "https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js", aA);
                        aa(aG, "https://oss.maxcdn.com/respond/1.4.2/respond.min.js", aA);
                    }
                }
            }
            if (O(aG, "preview_styles")) {
                V(window.document, ap() + "bootstrap_styles.css");
            }
        } else {
            if (j == 2) {
                if (O(aG, "jquery")) {
                    aa(aG, "//code.jquery.com/jquery-1.10.1.min.js", aA);
                }
                var aC = O(aG, "url");
                au(aG, aC + "css/foundation.min.css", aH);
                au(aG, aC + "css/foundation.min.css", aH);
                aa(aG, aC + "js/vendor/modernizr.js", aA);
                if (O(aG, "icons")) {
                    au(aG, O(aG, "url_icons"), aH);
                }
                if (af() == "tinymce") {
                    v(window.document, ".mce-panel button, .mce-panel button:hover { background-color: transparent !important; }" + ".mce-close { padding-left: 0 !important; padding-top: 0 !important; padding-right: 0 !important; }");
                }
                if (O(aG, "preview_styles")) {
                    V(window.document, ap() + "foundation_styles.css");
                }
            }
        }
        var aF = O(aG, "css");
        for (var aD = 0; aD < aF.length; aD++) {
            au(aG, aF[aD], aH);
        }
        var aB = O(aG, "js");
        for (var aD = 0; aD < aB.length; aD++) {
            aa(aG, aB[aD], aA);
        }
        if (j == 1 || j == 2) {
            var az = function (aI) {
                return (j == 1 && av(aI, "col-")) || (j == 2 && ax(aI, "columns")) || ax(aI, "row");
            };
            var ay = function (aM, aP, aN) {
                var aL = R(aM);
                if (aL != null) {
                    var aJ = az(aL);
                    if (!aJ && aL.parentElement != null) {
                        aJ = az(aL.parentElement);
                    }
                    if (aJ) {
                        var aI = aL.innerText;
                        var aK = aP == 8;
                        var aO = aP == 46;
                        if ((aK || aO) && aI.length == 1) {
                            aL.innerHTML = "&nbsp;";
                            D(aN);
                        }
                    }
                }
            };
            if (O(aG, "prevent_removing_divs") === true) {
                z(aG, "keyDown", ay);
            }
        }
    }
})();