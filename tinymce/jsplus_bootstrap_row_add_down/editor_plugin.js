(function () {
    function ax() {
        return "tinymce";
    }

    function i(aV) {
        return U() != "3" && aV.inline;
    }

    function H(aV) {
        return aV.id.replace(/\[/, "_").replace(/\]/, "_");
    }

    function k(aW) {
        if (U() == "3" || !i(aW)) {
            return aW.getContainer();
        }
        var aV = window.document.getElementById(aW.theme.panel._id);
        return aV;
    }

    function c(aV) {
        return aV.getDoc();
    }

    function V(aV) {
        return aV.getContent();
    }

    function Z(aW, aV) {
        aW.setContent(aV);
    }

    function ai(aV) {
        if (tinymce.activeEditor == null || tinymce.activeEditor.selection == null) {
            return null;
        }
        return tinymce.activeEditor.selection.getNode();
    }

    function ad() {
        return tinymce.baseURL;
    }

    function aM() {
        return j("jsplus_bootstrap_row_add_down");
    }

    function j(a0) {
        for (var aY = 0; aY < tinymce.editors.length; aY++) {
            var aZ = tinymce.editors[aY];
            var aX = ab(aZ, "external_plugins");
            if (typeof aX != "undefined" && typeof aX[a0] != "undefined") {
                var aW = aX[a0].replace("\\", "/");
                var aV = aW.lastIndexOf("/");
                if (aV == -1) {
                    aW = "";
                } else {
                    aW = aW.substr(0, aV) + "/";
                }
                return aW;
            }
        }
        return ad() + "/plugins/" + a0 + "/";
    }

    function U() {
        return tinymce.majorVersion == "4" ? 4 : 3;
    }

    function N() {
        return tinymce.minorVersion;
    }

    function z(aW, aV) {
        return window["jsplus_bootstrap_row_add_down_i18n"][aV];
    }

    function af(aW, aV) {
        return ab(aW, "jsplus_bootstrap_row_add_down_" + aV);
    }

    var an = {};

    function ab(aW, aV) {
        if (typeof(an[aV]) != "undefined") {
            return aW.getParam(aV, an[aV]);
        } else {
            return aW.getParam(aV);
        }
    }

    function y(aV, aW) {
        ah("jsplus_bootstrap_row_add_down_" + aV, aW);
    }

    function ah(aV, aW) {
        an[aV] = aW;
    }

    function aI(aW, aV) {
        if (U() == 4) {
            aW.insertContent(aV);
        } else {
            aW.execCommand("mceInsertContent", false, aV);
        }
    }

    function v() {
        return "";
    }

    var J = {};
    var aK = 0;

    function aQ(aX, aV) {
        var aW = H(aX) + "$" + aV;
        if (aW in J) {
            return J[aW];
        }
        return null;
    }

    function Y(aY, bg, bf, a6, a2, a9, be, a3, a0, aX, bc) {
        var bd = H(aY) + "$" + bg;
        if (bd in J) {
            return J[bd];
        }
        aK++;
        var a7 = "";
        var a4 = {};
        for (var a8 = a9.length - 1; a8 >= 0; a8--) {
            var aV = a9[a8];
            var a1 = H(aY) + "_jsplus_bootstrap_row_add_down_" + aK + "_" + a8;
            var aZ = null;
            if (aV.type == "ok") {
                aZ = -1;
            } else {
                if (aV.type == "cancel") {
                    aZ = -2;
                } else {
                    if (aV.type == "custom" && typeof(aV.onclick) != "undefined" && aV.onclick != null) {
                        aZ = aV.onclick;
                    }
                }
            }
            a4[a1] = aZ;
            if (U() == 3) {
                var ba = "border: 1px solid #b1b1b1;" + "border-color: rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25) rgba(0,0,0,.25);position: relative;" + "text-shadow: 0 1px 1px rgba(255,255,255,.75);" + "display: inline-block;" + "-webkit-border-radius: 3px;" + "-moz-border-radius: 3px;" + "border-radius: 3px;" + "-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "-moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "background-color: #f0f0f0;" + "background-image: -moz-linear-gradient(top,#fff,#d9d9d9);" + "background-image: -webkit-gradient(linear,0 0,0 100%,from(#fff),to(#d9d9d9));" + "background-image: -webkit-linear-gradient(top,#fff,#d9d9d9);" + "background-image: -o-linear-gradient(top,#fff,#d9d9d9);" + "background-image: linear-gradient(to bottom,#fff,#d9d9d9);" + "background-repeat: repeat-x;" + "filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffd9d9d9', GradientType=0);";
                if (aV.type == "ok") {
                    ba = "text-shadow: 0 1px 1px rgba(255,255,255,.75);" + "display: inline-block;" + "-webkit-border-radius: 3px;" + "-moz-border-radius: 3px;" + "border-radius: 3px;" + "-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "-moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);" + "min-width: 50px;" + "color: #fff;" + "border: 1px solid #b1b1b1;" + "border-color: rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25) rgba(0,0,0,.25);" + "background-color: #006dcc;" + "background-image: -moz-linear-gradient(top,#08c,#04c);" + "background-image: -webkit-gradient(linear,0 0,0 100%,from(#08c),to(#04c));" + "background-image: -webkit-linear-gradient(top,#08c,#04c);" + "background-image: -o-linear-gradient(top,#08c,#04c);" + "background-image: linear-gradient(to bottom,#08c,#04c);" + "background-repeat: repeat-x;" + "filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0044cc', GradientType=0);";
                }
                styleBtn = "-moz-box-sizing: border-box;" + "-webkit-box-sizing: border-box;" + "box-sizing: border-box;" + "padding: 4px 10px;" + "font-size: 14px;" + "line-height: 20px;" + "cursor: pointer;" + "text-align: center;" + "overflow: visible;" + "-webkit-appearance: none;" + "background: none;" + "border: none;";
                if (aV.type == "ok") {
                    styleBtn += "color: #fff;text-shadow: 1px 1px #333;";
                }
                a7 += '<div tabindex="-1" style="' + ba + "position:relative;float:right;top: 10px;height: 28px;margin-right:15px;text-align:center;" + '">' + '<button id="' + a1 + '" type="button" tabindex="-1" style="' + styleBtn + "height:100%" + '">' + ap(aV.title) + "</button>" + "</div>";
            } else {
                a7 += '<div class="mce-widget mce-btn ' + (aV.type == "ok" ? "mce-primary" : "") + ' mce-abs-layout-item" tabindex="-1" style="position:relative;float:right;top: 10px;height: 28px;margin-right:15px;text-align:center">' + '<button id="' + a1 + '" type="button" tabindex="-1" style="height: 100%;">' + ap(aV.title) + "</button>" + "</div>";
            }
        }
        if (U() == 3) {
            var a5 = '<div style="display: none; position: fixed; height: 100%; width: 100%;top:0;left:0;z-index:19000" data-popup-id="' + bd + '">' + '<div style="position: absolute; height: 100%; width: 100%;top:0;left:0;background-color: gray;opacity: 0.3;z-index:-1"></div>' + '<div class="mce_dlg_jsplus_bootstrap_row_add_down" style="display: table-cell; vertical-align: middle;z-index:19005">' + '<div class="" ' + 'style="' + "border-width: 1px; margin-left: auto; margin-right: auto; width: " + a6 + "px;" + "-webkit-border-radius: 6px;-moz-border-radius: 6px;border-radius: 6px;-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);" + "box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);background: transparent;background: #fff;" + "-webkit-transition: opacity 150ms ease-in;transition: opacity 150ms ease-in;" + "border: 0 solid #9e9e9e;background-repeat:repeat-x" + '">' + '<div style="padding: 9px 15px;border-bottom: 1px solid #c5c5c5;position: relative;">' + '<div style="line-height: 20px;font-size: 20px;font-weight: 700;text-rendering: optimizelegibility;padding-right: 10px;">' + ap(bf) + "</div>" + '<button style="position: absolute;right: 15px;top: 9px;font-size: 20px;font-weight: 700;line-height: 20px;color: #858585;cursor: pointer;height: 20px;overflow: hidden;background: none;border: none;padding-top: 0 !important; padding-right: 0 !important;padding-left: 0 !important" type="button" id="' + H(aY) + "_jsplus_bootstrap_row_add_down_" + aK + '_close">×</button>' + "</div>" + '<div style="overflow:hidden">' + a2 + '<div hidefocus="1" tabindex="-1" ' + 'style="border-width: 1px 0px 0px; left: 0px; top: 0px; height: 50px;' + "display: block;background-color: #fff;border-top: 1px solid #c5c5c5;-webkit-border-radius: 0 0 6px 6px;-moz-border-radius: 0 0 6px 6px;border-radius: 0 0 6px 6px;" + "border: 0 solid #9e9e9e;background-color: #f0f0f0;background-image: -moz-linear-gradient(top,#fdfdfd,#ddd);background-image: -webkit-gradient(linear,0 0,0 100%,from(#fdfdfd),to(#ddd));" + "background-image: -webkit-linear-gradient(top,#fdfdfd,#ddd);background-image: -o-linear-gradient(top,#fdfdfd,#ddd);" + "background-image: linear-gradient(to bottom,#fdfdfd,#ddd);background-repeat: repeat-x;" + '">' + '<div class="mce-container-body mce-abs-layout" style="height: 50px;">' + '<div class="mce-abs-end"></div>' + a7 + "</div>" + "</div>" + "</div>" + "</div>" + "</div>" + "</div>";
        } else {
            var a5 = '<div style="display: none; font-family:Arial; position: fixed; height: 100%; width: 100%;top:0;left:0;z-index:19000" data-popup-id="' + bd + '">' + '<div style="position: absolute; height: 100%; width: 100%;top:0;left:0;background-color: gray;opacity: 0.3;z-index:-1"></div>' + '<div class="mce_dlg_jsplus_bootstrap_row_add_down" style="display: table-cell; vertical-align: middle;z-index:19005">' + '<div class="" ' + 'style="' + "border-width: 1px; margin-left: auto; margin-right: auto; width: " + a6 + "px;" + "-webkit-border-radius: 6px;-moz-border-radius: 6px;border-radius: 6px;-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);" + "box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);background: transparent;background: #fff;" + "-webkit-transition: opacity 150ms ease-in;transition: opacity 150ms ease-in;" + "border: 0 solid #9e9e9e;background-repeat:repeat-x" + '">' + '<div  class="mce-window-head">' + '<div class="mce-title">' + ap(bf) + "</div>" + '<button class="mce-close" type="button" id="' + H(aY) + "_jsplus_bootstrap_row_add_down_" + aK + '_close" style="background:none;border:none">×</button>' + "</div>" + '<div class="mce-container-body mce-abs-layout">' + a2 + '<div class="mce-container mce-panel mce-foot" hidefocus="1" tabindex="-1" style="border-width: 1px 0px 0px; left: 0px; top: 0px; height: 50px;">' + '<div class="mce-container-body mce-abs-layout" style="height: 50px;">' + '<div class="mce-abs-end"></div>' + a7 + "</div>" + "</div>" + "</div>" + "</div>" + "</div>";
        }
        var aW = av(a5)[0];
        var bb = {
            $: aW, appendedToDOM: false, num: aK, editor: aY, open: function () {
                if (!this.appendedToDOM) {
                    this.editor.getElement().parentNode.appendChild(this.$);
                    var bj = this;
                    for (var bk in a4) {
                        var bh = a4[bk];
                        if (bh != null) {
                            var bi = document.getElementById(bk);
                            if (bh === -1) {
                                bi.onclick = function () {
                                    bj.ok();
                                };
                            } else {
                                if (bh === -2) {
                                    bi.onclick = function () {
                                        bj.cancel();
                                    };
                                } else {
                                    bi.onclick = function () {
                                        bh();
                                    };
                                }
                            }
                        }
                    }
                    document.getElementById(H(this.editor) + "_jsplus_bootstrap_row_add_down_" + this.num + "_close").onclick = function () {
                        bj.cancel();
                    };
                    this.appendedToDOM = true;
                    if (bc != null) {
                        bc(this.editor);
                    }
                }
                if (a0 != null) {
                    a0(this.editor);
                }
                this.$.style.display = "table";
            }, close: function () {
                this.$.style.display = "none";
                if (aX != null) {
                    aX(this.editor);
                }
            }, ok: function () {
                if (be != null) {
                    if (be(this.editor) === false) {
                        return;
                    }
                }
                bb.close();
            }, cancel: function () {
                if (a3 != null) {
                    if (a3(this.editor) === false) {
                        return;
                    }
                }
                this.close();
            }
        };
        J[bd] = bb;
        return bb;
    }

    var e = {};
    var ay = 0;

    function aj(aW) {
        var aV = H(aW);
        if (aV in e) {
            return e[aV];
        }
        return null;
    }

    function aT(a2, aW, a0, aY, a4, a3) {
        var a5 = H(a2);
        if (a5 in e) {
            return e[a5];
        }
        ay++;
        var aZ = "";
        if (U() == 3) {
            aZ = "<div" + ' style="margin-left:-11px;background: #FFF;border: 1px solid gray;z-index: 165535;padding:8px 12px 8px 8px;position:absolute' + (aW != null ? (";width:" + aW + "px") : "") + '">' + a0 + "</div>";
        } else {
            aZ = "<div" + ' class="mce-container mce-panel mce-floatpanel mce-popover mce-bottom mce-start"' + ' style="z-index: 165535;padding:8px 12px 8px 8px' + (aW != null ? (";width:" + aW + "px") : "") + '">' + '<div class="mce-arrow" hidefocus="1" tabindex="-1" role="dialog"></div>' + a0 + "</div>";
        }
        var a1 = '<div style="z-index:165534;position:absolute;left:0;top:0;width:100%;height:100%;display:none" data-popup-id="' + a5 + '">' + aZ + "</div>";
        var aX = av(a1)[0];
        var aV = {
            $_root: aX, $_popup: aX.children[0], num: ay, appendedToDOM: false, editor: a2, open: function () {
                if (!this.appendedToDOM) {
                    this.$_root.onclick = (function () {
                        return function (bg) {
                            e[this.getAttribute("data-popup-id")].close();
                            bg.stopPropagation();
                        };
                    })();
                    this.$_popup.onclick = function (bg) {
                        bg.stopPropagation();
                    };
                    k(this.editor).appendChild(this.$_root);
                    var bc = this;
                    this.appendedToDOM = true;
                    if (a3 != null) {
                        a3(this.editor);
                    }
                }
                if (aY != null) {
                    aY(this.editor);
                }
                var ba = k(this.editor);
                var bf = ba.getElementsByClassName("mce_jsplus_bootstrap_row_add_down");
                if (bf.length == 0) {
                    bf = ba.getElementsByClassName("mce-jsplus_bootstrap_row_add_down");
                }
                if (bf.length == 0) {
                    console.log("Unable to find button with class 'mce_jsplus_bootstrap_row_add_down' or 'mce-jsplus_bootstrap_row_add_down' for editor " + H(this.editor));
                } else {
                    var a6 = bf[0];
                    var be = a6.getBoundingClientRect();
                    var bd = function (bh, bg) {
                        var bj = 0, bi = 0;
                        do {
                            bj += bh.offsetTop || 0;
                            bi += bh.offsetLeft || 0;
                            bh = bh.offsetParent;
                        } while (bh && bh != bg);
                        return {top: bj, left: bi};
                    };
                    var a7 = k(this.editor);
                    var a8 = bd(a6, a7);
                    this.$_popup.style.top = (a8.top + a6.offsetHeight) + "px";
                    this.$_popup.style.left = (a8.left + a6.offsetWidth / 2) + "px";
                    this.$_popup.style.display = "block";
                    var bb = document.body;
                    var a9 = document.documentElement;
                    this.$_root.style.height = Math.max(bb.scrollHeight, bb.offsetHeight, a9.clientHeight, a9.scrollHeight, a9.offsetHeight);
                    this.$_root.style.display = "block";
                }
            }, close: function () {
                this.$_popup.style.display = "none";
                this.$_root.style.display = "none";
                if (a4 != null) {
                    a4(this.editor);
                }
            }
        };
        e[a5] = aV;
        return aV;
    }

    var s = {};

    function ae(aZ, a5, a2, a3, a0, a1, a4) {
        var aX = (function () {
            var a6 = aZ;
            return function (a7) {
                a1(a6);
            };
        })();
        var aY = aZ;
        var aW = function (a6, a7) {
            if (!(H(a6) in s)) {
                s[H(a6)] = {};
            }
            s[H(a6)][a5] = a7;
            if (a0) {
                tinymce.DOM.remove(a7.getEl("preview"));
            }
            if (a1 != null) {
                a7.on("click", aX);
            }
            if (a4) {
                a4(a6);
            }
        };
        var aV = {
            text: "",
            type: "button",
            icon: true,
            classes: "widget btn jsplus_bootstrap_row_add_down btn-jsplus_bootstrap_row_add_down-" + H(aZ),
            image: a2,
            label: a3,
            tooltip: a3,
            title: a3,
            id: "btn-" + a5 + "-" + H(aZ),
            onPostRender: function () {
                aW(aY, this);
            }
        };
        if (a0) {
            aV.type = U() == "3" ? "ColorSplitButton" : "colorbutton";
            aV.color = "#FFFFFF";
            aV.panel = {};
        }
        if (U() == "3" && a0) {
            (function () {
                var a6 = false;
                aZ.onNodeChange.add(function (bd, a8, a9) {
                    if (a6) {
                        return;
                    }
                    a6 = true;
                    var bb = k(bd);
                    var bc = bb.getElementsByClassName("mce_" + a5);
                    if (bc.length > 0) {
                        var a7 = bc[0];
                        var be = a7.parentNode;
                        var ba = be.nextSibling;
                        var bg = av('<div id="content_forecolor" role="button" tabindex="-1" aria-labelledby="content_forecolor_voice" aria-haspopup="true">' + '<table role="presentation" class="mceSplitButton mceSplitButtonEnabled mce_forecolor" cellpadding="0" cellspacing="0" title="Select Text Color">' + "<tbody>" + "<tr>" + '<td class="mceFirst">' + "</td>" + '<td class="mceLast">' + '<a role="button" style="width:10px" tabindex="-1" href="javascript:;" class="mceOpen mce_forecolor" onclick="return false;" onmousedown="return false;" title="Select Text Color">' + '<span class="mceOpen mce_forecolor">' + '<span style="display:none;" class="mceIconOnly" aria-hidden="true">▼</span>' + "</span>" + "</a>" + "</td>" + "</tr>" + "</tbody>" + "</table>" + "</div>")[0];
                        var bf = bg.getElementsByClassName("mceFirst")[0];
                        be.appendChild(bg);
                        bf.appendChild(a7);
                        a7.style.marginRight = "-1px";
                        a7.className = a7.className + " mceAction mce_forecolor";
                        bg.getElementsByClassName("mceOpen")[0].onclick = aX;
                    }
                });
            })();
        }
        aZ.addButton(a5, aV);
    }

    var aa = 0;
    var M = 1;
    var T = 2;

    function t(aW, aY, aX) {
        if (aX != aa && aX != M && aX != T) {
            return;
        }
        if (U() == 3) {
            aW.controlManager.setDisabled(aY, aX == aa);
            aW.controlManager.setActive(aY, aX == T);
        } else {
            if ((H(aW) in s) && (aY in s[H(aW)])) {
                var aV = s[H(aW)][aY];
                aV.disabled(aX == aa);
                aV.active(aX == T);
            }
        }
    }

    function W(aV, aW) {
        if (U == 3) {
            aV.onNodeChange.add(function (aY, aX, aZ) {
                aW(aY);
            });
        } else {
            aV.on("NodeChange", function (aX) {
                aW(aX.target);
            });
        }
    }

    function K(aW, aV, aX) {
        if (aV == "mode") {
            return;
        }
        if (aV == "beforeGetOutputHTML") {
            aW.on("SaveContent", function (aY) {
                aY.content = aX(aW, aY.content);
            });
            return;
        }
        if (aV == "contentDom") {
            if (U() == 4) {
                aW.on("init", function (aY) {
                    aX(aW);
                });
            } else {
                aW.onInit.add(function (aY) {
                    aX(aY);
                });
            }
            return;
        }
        if (aV == "elementsPathUpdate") {
            return;
        }
        if (aV == "selectionChange") {
            if (U == 3) {
                aW.onNodeChange.add(function (aZ, aY, a0) {
                    aX(aZ);
                });
            } else {
                aW.on("NodeChange", function (aY) {
                    aX(aY.target);
                });
            }
        }
        if (aV == "keyDown") {
            aW.on("keydown", (function () {
                var aZ = aW;
                var aY = aX;
                return function (a0) {
                    aY(aZ, a0.keyCode, a0);
                };
            })());
        }
    }

    function S(aV) {
        aV.preventDefault();
    }

    function A(aY, a4, a0, aX, a1, aV, aW) {
        var a2 = z(aY, aX.replace(/^jsplus_/, "").replace(/^jsplus_/, ""));
        var a3 = "";
        if (aW && aW != null && ab(aY, aW) === true) {
            a3 += "_bw";
        }
        var aZ = aM() + "mce_icons/" + a0 + v() + a3 + ".png";
        ae(aY, a4, aZ, a2, false, a1);
        if (aV && U() > 3) {
            aY.addMenuItem(a4, {text: a2, context: aV, icon: true, image: aZ});
        }
    }

    function u(aV) {
        return true;
    }

    function ar(aW, aV, aX) {
        if (aV != null && aV != "") {
            tinymce.PluginManager.requireLangPack(aW);
        }
        tinymce.PluginManager.add(aW, function (aZ, aY) {
            aX(aZ);
        });
    }

    function d() {
        var aV = '<button type="button" class="jsdialog_x mce-close"><i class="mce-ico mce-i-remove"></i></button>';
        if (N().indexOf("0.") === 0 || N().indexOf("1.") === 0 || N().indexOf("2.") === 0) {
            aV = '<button type="button" class="jsdialog_x mce-close">×</button>';
        }
        JSDialog.Config.skin = null;
        JSDialog.Config.templateDialog = '<div class="jsdialog_plugin_jsplus_bootstrap_row_add_down jsdialog_dlg mce-container mce-panel mce-floatpanel mce-window mce-in" hidefocus="1">' + '<div class="mce-reset">' + '<div class="jsdialog_title mce-window-head">' + '<div class="jsdialog_title_text mce-title"></div>' + aV + "</div>" + '<div class="jsdialog_content_wrap mce-container-body mce-window-body">' + '<div class="mce-container mce-form mce-first mce-last">' + '<div class="jsdialog_content mce-container-body">' + "</div>" + "</div>" + "</div>" + '<div class="mce-container mce-panel mce-foot" hidefocus="1">' + '<div class="jsdialog_buttons mce-container-body">' + "</div>" + "</div>" + "</div>" + "</div>";
        JSDialog.Config.templateButton = (N().indexOf("0.") === 0 || N().indexOf("1.") === 0 || N().indexOf("2.") === 0) ? '<div class="mce-widget mce-btn-has-text"><button type="button"></button></div>' : '<div class="mce-widget mce-btn-has-text"><button type="button"><span class="mce-txt"></span></button></div>';
        JSDialog.Config.templateBg = '<div class="jsdialog_plugin_jsplus_bootstrap_row_add_down jsdialog_bg"></div>';
        JSDialog.Config.classButton = "mce-btn";
        JSDialog.Config.classButtonOk = "mce-primary";
        JSDialog.Config.contentBorders = [3, 1, 15, 1, 73];
        C(document, ".jsdialog_plugin_jsplus_bootstrap_row_add_down.jsdialog_bg { background-color: black; opacity: 0.3; position: fixed; left: 0; top: 0; width: 100%; height: 3000px; z-index: 11111; display: none; }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down.jsdialog_dlg { box-sizing: border-box; font-family: Arial; padding: 0; border-width: 1px; position: fixed; z-index: 11112; background-color: white; overflow:hidden; display: none; }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down.jsdialog_show { display: block; }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down .mce-foot { height: 50px; }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down .mce-foot .jsdialog_buttons { padding: 10px; }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down .mce-btn-has-text { float: right; margin-left: 5px; text-align: center; }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down .jsdialog_message_contents { font-size: 16px; padding: 10px 0 10px 7px; display: table; overflow: hidden; }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down .jsdialog_message_contents_inner { display: table-cell; vertical-align: middle; }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down .jsdialog_message_icon { padding-left: 100px; min-height: 64px; background-position: 10px 10px; background-repeat: no-repeat; box-sizing: content-box; }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down .jsdialog_message_icon_info { background-image: url(info.png); }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down .jsdialog_message_icon_warning { background-image: url(warning.png); }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down .jsdialog_message_icon_error { background-image: url(error.png); }" + ".jsdialog_plugin_jsplus_bootstrap_row_add_down .jsdialog_message_icon_confirm { background-image: url(confirm.png); }");
    }

    var q = 201;
    var n = null;
    var m = null;
    var l = [];
    var aJ;
    var aL;
    var aF;
    var I = "";
    var D = "";
    var G = "";
    var aw = {};
    if (q == 20 || q == 21 || q == 22) {
        y("use_dialog", false);
        y("show_options", true);
        y("show_advanced_options", true);
        if (q == 20) {
            y("default_width_100", false);
        }
        y("default_add_header", false);
        y("default_class", "");
        y("default_style", "");
        if (q == 21) {
            y("default_striped", true);
            y("default_bordered", false);
            y("default_condensed", false);
        }
        l = [];
        var ac = "border:1px solid rgb(171, 207, 255);background-color:rgb(195, 217, 255)";
        if (q == 21) {
            ac = "border:1px solid #6f5499;background-color:#9174C0";
        } else {
            if (q == 22) {
                ac = "border:1px solid #007095;background-color:#008cba";
            }
        }
        for (var az = 0; az < 6; az++) {
            for (var L = 0; L < 6; L++) {
                l.push(['<div id="jsplus_bootstrap_row_add_down_cell_' + L + "_" + az + '_%id%" style="width:20px;height:20px;margin:1px;float:left;' + ac + '"></div>', L + "," + az, "table"]);
            }
        }
        I = '<table style="border-width:0px;background-color:transparent;width:%table_width%px">' + "<tbody>" + "<tr>" + '<td style="box-sizing: content-box;width: 154px;vertical-align: top;line-height:0">';
        G = "</td>" + '<td style="box-sizing: content-box;width:%options_width%px;vertical-align: top;padding-top:6px;padding-left:15px" id="jsplus_bootstrap_row_add_down_options_%id%">' + (q == 20 ? '<div style="height:20px"><label style="font-size:13px;cursor:pointer"><input style="vertical-align:middle;margin:-2px 5px 0 0;cursor:pointer" type="checkbox" id="jsplus_bootstrap_row_add_down_width_100_%id%"/>{{jsplus_table_width_100}}</label></div>' : "") + '<div style="height:20px"><label style="font-size:13px;cursor:pointer"><input type="checkbox" style="vertical-align:middle;margin:-2px 5px 0 0;cursor:pointer" id="jsplus_bootstrap_row_add_down_add_header_%id%"/>{{jsplus_table_add_header}}</label></div>' + (q == 21 ? '<div style="height:20px"><label style="font-size:13px;cursor:pointer"><input type="checkbox" style="vertical-align:middle;margin:-2px 5px 0 0;cursor:pointer" id="jsplus_bootstrap_row_add_down_striped_%id%"/>{{jsplus_bootstrap_table_new_striped}}</label></div>' + '<div style="height:20px"><label style="font-size:13px;cursor:pointer"><input type="checkbox" style="vertical-align:middle;margin:-2px 5px 0 0;cursor:pointer" id="jsplus_bootstrap_row_add_down_bordered_%id%"/>{{jsplus_bootstrap_table_new_bordered}}</label></div>' + '<div style="height:20px"><label style="font-size:13px;cursor:pointer"><input type="checkbox" style="vertical-align:middle;margin:-2px 5px 0 0;cursor:pointer" id="jsplus_bootstrap_row_add_down_condensed_%id%"/>{{jsplus_bootstrap_table_new_condensed}}</label></div>' : "") + '<div id="jsplus_bootstrap_row_add_down_advanced_options_%id%">' + '<div style="height:25px;margin-top:7px"><label style="width:100%;font-size:13px;line-height:24px;white-space:normal">{{jsplus_table_class}}<input style="width:100px;float:right;border:rgb(168, 168, 168) solid 1px;padding:1px 0;margin:2px 0;font-size:13px;cursor:pointer" id="jsplus_bootstrap_row_add_down_class_%id%"/></label></div>' + '<div style="height:25px"><label style="width:100%;font-size:13px;line-height:24px;white-space:normal">{{jsplus_table_style}}<input style="width:100px;float:right;border:rgb(168, 168, 168) solid 1px;padding:1px 0;margin:2px 0;font-size:13px;cursor:pointer" id="jsplus_bootstrap_row_add_down_style_%id%"/></label></div>' + "</div>" + "</td>" + "</tr>" + "</tbody>" + "</table>";
        if (q == 20) {
            aw["width_100"] = true;
        }
        aw["add_header"] = false;
        aw["class"] = "";
        aw["style"] = "";
        if (q == 21) {
            aw["striped"] = true;
            aw["bordered"] = false;
            aw["condensed"] = false;
        }
    } else {
        if (q == 3) {
            y("use_dialog", false);
            ah("jsplus_bootstrap_row_add_down", []);
            y("popup_width", 400);
            y("dialog_width", 400);
            y("dialog_height", 400);
        } else {
            if (q == 0) {
                y("use_dialog", false);
                aJ = 360;
                aL = 384;
                aF = 335;
                l = [['<img style="padding:7px;width:106px;height:76px" src="{path}/img/templates/w33w67.png"/>', '<div class="cke_show_border" style="width:100%"><div class="cke_show_border" style="float:left;width:33%"><p>33%</p></div><div class="cke_show_border" style="float:left;width:67%"><p>67%</p></div></div>', "html"], ['<img style="padding:7px;width:106px;height:76px" src="{path}/img/templates/w50w50.png"/>', '<div class="cke_show_border" style="width:100%"><div class="cke_show_border" style="float:left;width:50%"><p>50%</p></div><div class="cke_show_border" style="float:left;width:50%"><p>50%</p></div></div>', "html"], ['<img style="padding:7px;width:106px;height:76px" src="{path}/img/templates/w67w33.png"/>', '<div class="cke_show_border" style="width:100%"><div class="cke_show_border" style="float:left;width:67%"><p>67%</p></div><div class="cke_show_border" style="float:left;width:33%"><p>33%</p></div></div>', "html"], ['<img style="padding:7px;width:106px;height:76px" src="{path}/img/templates/w25w25w50.png"/>', '<div class="cke_show_border" style="width:100%"><div class="cke_show_border" style="float:left;width:25%"><p>25%</p></div><div class="cke_show_border" style="float:left;width:25%"><p>25%</p></div><div class="cke_show_border" style="float:left;width:50%"><p>50%</p></div></div>', "html"], ['<img style="padding:7px;width:106px;height:76px" src="{path}/img/templates/w25w25w25w25.png"/>', '<div class="cke_show_border" style="width:100%"><div class="cke_show_border" style="float:left;width:25%"><p>25%</p></div><div class="cke_show_border" style="float:left;width:25%"><p>25%</p></div><div class="cke_show_border" style="float:left;width:25%"><p>25%</p></div><div class="cke_show_border" style="float:left;width:25%"><p>25%</p></div></div>', "html"], ['<img style="padding:7px;width:106px;height:76px" src="{path}/img/templates/w50w25w25.png"/>', '<div class="cke_show_border" style="width:100%"><div class="cke_show_border" style="float:left;width:50%"><p>50%</p></div><div class="cke_show_border" style="float:left;width:25%"><p>25%</p></div><div class="cke_show_border" style="float:left;width:25%"><p>25%</p></div></div>', "html"], ['<img style="padding:7px;width:106px;height:76px" src="{path}/img/templates/imgl.png"/>', '<div class="cke_show_border" style="width:100%;text-align:left"><p><img src="{path}/img/templates/sample.png" style="float:left;padding:0 20px 20px 0">Text text text</p></div>', "html"], ['<img style="padding:7px;width:106px;height:76px" src="{path}/img/templates/w25w50w25.png"/>', '<div class="cke_show_border" style="width:100%"><div class="cke_show_border" style="float:left;width:25%"><p>25%</p></div><div class="cke_show_border" style="float:left;width:50%"><p>50%</p></div><div class="cke_show_border" style="float:left;width:25%"><p>25%</p></div></div>', "html"], ['<img style="padding:7px;width:106px;height:76px" src="{path}/img/templates/imgr.png"/>', '<div class="cke_show_border" style="width:100%;text-align:right"><p>Text text text<img src="{path}/img/templates/sample.png" style="float:right;padding:0 0 20px 20px"></p></div>', "html"]];
            } else {
                if (q == 1 || q == 101 || q == 201) {
                    y("use_dialog", false);
                    y("default_col_type", "xs");
                    aJ = 500;
                    aL = 675;
                    aF = 215;
                    aw["col-type"] = "xs";
                    I = '<table style="border-width:0px;background-color:transparent;width:100%">' + "<tbody>" + "<tr>" + '<td style="box-sizing: content-box;width: 75px;vertical-align: top;padding-right: 10px;color: #444;">' + '<div style="padding-top: 20px;padding-right: 15px;font-size: 12px;text-align: right;display:none">Columns:</div>' + '<div style="margin-top: 17px;padding-right: 15px;font-size: 12px;text-align: right">Rows:</div>' + "</td>" + '<td style="box-sizing: content-box;">';
                    G = "</td>" + '<td style="box-sizing:content-box;width:90px;font-size:12px;font-family:Arial;padding-left:20px;vertical-align:middle;display:none">' + '<div style="margin-bottom: 12px;height: 20px;width: 100%">Column type:</div>' + '<div style="padding-bottom:7px">' + '<label style="font-weight:normal;margin-bottom:0;cursor:pointer;white-space: normal" id="jsplus_bootstrap_row_add_downcolTypeXS_label_%id%">' + '<input data-col="xs" style="margin-left:10px;margin-top:-1px;vertical-align:middle;display:inline" type="radio" name="jsplus_bootstrap_row_add_down_radio_%id%"/>' + "&nbsp;XS" + "</label>" + "</div>" + '<div style="padding-bottom:7px">' + '<label style="font-weight:normal;margin-bottom:0;cursor:pointer;white-space: normal;display:inline" id="jsplus_bootstrap_row_add_downcolTypeSM_label_%id%">' + '<input data-col="sm" style="margin-left:10px;margin-top:-1px;vertical-align:middle;" type="radio" name="jsplus_bootstrap_row_add_down_radio_%id%"/>' + "&nbsp;SM" + "</label>" + "</div>" + '<div style="padding-bottom:7px">' + '<label style="font-weight:normal;margin-bottom:0;cursor:pointer;white-space: normal;display:inline" id="jsplus_bootstrap_row_add_downcolTypeMD_label_%id%">' + '<input data-col="md" style="margin-left:10px;margin-top:-1px;vertical-align:middle;" type="radio" name="jsplus_bootstrap_row_add_down_radio_%id%"/>' + "&nbsp;MD" + "</label>" + "</div>" + '<div style="padding-bottom:7px">' + '<label style="font-weight:normal;margin-bottom:0;cursor:pointer;white-space: normal;display:inline" id="jsplus_bootstrap_row_add_downcolTypeLG_label_%id%">' + '<input data-col="lg" style="margin-left:10px;margin-top:-1px;vertical-align:middle;" type="radio" ' + (aw["col-type"] == "lg" ? "checked" : "") + ' name="jsplus_bootstrap_row_add_down_radio_%id%"/>' + "&nbsp;LG" + "</label>" + "</div>" + '<div style="padding-bottom:7px" id="jsplus_bootstrap_row_add_down_div_xl_%id%">' + '<label style="font-weight:normal;margin-bottom:0;cursor:pointer;white-space: normal;display:inline" id="jsplus_bootstrap_row_add_downcolTypeXL_label_%id%">' + '<input data-col="xl" style="margin-left:10px;margin-top:-1px;vertical-align:middle;" type="radio" ' + (aw["col-type"] == "xl" ? "checked" : "") + ' name="jsplus_bootstrap_row_add_down_radio_%id%"/>' + "&nbsp;XL" + "</label>" + "</div>" + "</td>" + "</tr>" + "</tbody>" + "</table>";
                    l = [
                            ['<div style="width:36px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:12px;margin:0 auto;line-height:35px;height:36px;">1</div></div>', '<div class="col-xs-1"><p>col-xs-1</p></div>', "html"],
                            ['<div style="width:36px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:14px;margin:0 auto;line-height:35px;height:36px;">2</div></div>', '<div class="col-xs-2"><p>col-xs-2</p></div>', "html"],
                            ['<div style="width:36px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:16px;margin:0 auto;line-height:35px;height:36px;">3</div></div>', '<div class="col-xs-3"><p>col-xs-3</p></div>', "html"],
                            ['<div style="width:36px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:18px;margin:0 auto;line-height:35px;height:36px;">4</div></div>', '<div class="col-xs-4"><p>col-xs-4</p></div>', "html"],
                            ['<div style="width:36px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:20px;margin:0 auto;line-height:35px;height:36px;">5</div></div>', '<div class="col-xs-5"><p>col-xs-5</p></div>', "html"],
                            ['<div style="width:36px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:22px;margin:0 auto;line-height:35px;height:36px;">6</div></div>', '<div class="col-xs-6"><p>col-xs-6</p></div>', "html"],
                            ['<div style="width:36px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:24px;margin:0 auto;line-height:35px;height:36px;">7</div></div>', '<div class="col-xs-7"><p>col-xs-7</p></div>', "html"],
                            ['<div style="width:36px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:26px;margin:0 auto;line-height:35px;height:36px;">8</div></div>', '<div class="col-xs-8"><p>col-xs-8</p></div>', "html"],
                            ['<div style="width:35px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:27px;margin:0 auto;line-height:35px;height:36px;">9</div></div>', '<div class="col-xs-9"><p>col-xs-9</p></div>', "html"],
                            ['<div style="width:35px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:29px;margin:0 auto;line-height:35px;height:36px;">10</div></div>', '<div class="col-xs-10"><p>col-xs-10</p></div>', "html"],
                            ['<div style="width:34px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:30px;margin:0 auto;line-height:35px;height:36px;">11</div></div>', '<div class="col-xs-11"><p>col-xs-11</p></div>', "html"],
                            ['<div style="width:34px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:32px;margin:0 auto;line-height:35px;height:36px;">12</div></div>', '<div class="col-xs-12"><p>col-xs-12</p></div>', "html"],
                            ['<div style="width:100px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:38px;padding:0px;box-sizing:border-box!important;"> 1col<br/>(narrow width) </div></div>', '<div class="width-900 single-col"><p><br data-mce-bogus="1"></p></div>', "html"],
                            ['<div style="width:100px;height:36px;padding-bottom:8px;padding-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:38px;padding:9px 0;box-sizing:border-box!important;"> 2col </div></div>', '<div class="width-900"><div class="flexbox"><div class="col col-50 col-s-full"><p><br data-mce-bogus="1"></p></div><div class="col col-50 col-s-full"><p><br data-mce-bogus="1"></p></div></div></div>', "html"],
                            ['<div style="width:100px;height:36px;padding-bottom:8px;padding-top:8px;"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:38px;padding:9px 0;box-sizing:border-box!important;"> 3col </div></div>', '<div class="flexbox"><div class="col col-33 col-m-full first"><p><br data-mce-bogus="1"></p></div><div class="col col-33 col-m-full"><p><br data-mce-bogus="1"></p></div><div class="col col-33 col-m-full last"><p><br data-mce-bogus="1"></p></div></div>', "html"],
                            ['<div style="width:100px;height:36px;padding-bottom:8px;padding-top:8px;"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:38px;padding:9px 0;box-sizing:border-box!important;"> 4col </div></div>', '<div class="flexbox"><div class="col col-25 col-m-50 col-s-full first"><p><br data-mce-bogus="1"></p></div><div class="col col-25 col-m-50 col-s-full"><p><br data-mce-bogus="1"></p></div><div class="col col-25 col-m-50 col-s-full"><p><br data-mce-bogus="1"></p></div><div class="col col-25 col-m-50 col-s-full last"><p><br data-mce-bogus="1"></p></div></div>', "html"],
                            ['<div style="width:100px;height:36px;padding-bottom:8px;padding-top:8px;"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:38px;padding:0px;box-sizing:border-box!important;"> 2col<br/>(narrow left)</div></div>', '<div class="flexbox"><div class="col col-33 col-m-full first"><p><br data-mce-bogus="1"></p></div><div class="col col-66 col-m-full"><p><br data-mce-bogus="1"></p></div></div>', "html"],
                            ['<div style="width:100px;height:36px;padding-bottom:8px;padding-top:8px;"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:38px;padding:0px;box-sizing:border-box!important;"> 2col<br/>(narrow right)</div></div>','<div class="flexbox"><div class="col col-66 col-m-full first"><p><br data-mce-bogus="1"></p></div><div class="col col-33 col-m-full"><p><br data-mce-bogus="1"></p></div></div>', "html"],
                            ['<div style="width:100px;height:36px;padding-bottom:8px;padding-top:8px;"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:38px;padding:0px;box-sizing:border-box!important;"> 2col<br/>(thin left)</div></div>', '<div class="flexbox"><div class="col col-25 col-m-full first"><p><br data-mce-bogus="1"></p></div><div class="col col-75 col-m-full"><p><br data-mce-bogus="1"></p></div></div>', "html"],
                            ['<div style="width:100px;height:36px;padding-bottom:8px;padding-top:8px;"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:38px;padding:0px;box-sizing:border-box!important;"> 2col<br/>(thin right)</div></div>','<div class="flexbox"><div class="col col-75 col-m-full first"><p><br data-mce-bogus="1"></p></div><div class="col col-25 col-m-full"><p><br data-mce-bogus="1"></p></div></div>', "html"]
                        ];
                } else {
                    if (q == 2 || q == 102 || q == 202) {
                        y("use_dialog", false);
                        y("default_col_type", "small");
                        aJ = 660;
                        aL = 675;
                        aF = 215;
                        aw["col-type"] = "small";
                        I = '<table style="border-width:0px;background-color:transparent;width:100%">' + "<tbody>" + "<tr>" + '<td style="box-sizing: content-box;width: 75px;vertical-align: top;padding-right: 10px;color: #444;">' + '<div style="padding-top: 20px;padding-right: 15px;font-size: 12px;text-align: right;display:none">Columns:</div>' + '<div style="margin-top: 17px;padding-top: 46px;padding-right: 15px;font-size: 12px;text-align: right">Rows:</div>' + "</td>" + '<td style="box-sizing: content-box;">';
                        G = '</td><td style="width:90px;font-size:12px;font-family:Arial;padding-left:20px;line-height:20px;vertical-align:middle">' + '<div style="margin-bottom: 7px;height: 20px;width: 100%;display: inline-block;">Column type:</div><br/>' + '<label style="font-weight:normal;margin-bottom:0;cursor:pointer;white-space: normal;display:inline" id="jsplus_bootstrap_row_add_downcolTypesmall_label_%id%"><input data-col="small" style="margin-left:15px;" type="radio" ' + (aw["col-type"] == "small" ? "checked" : "") + ' name="jsplus_bootstrap_row_add_down_radio_%id%">&nbsp;Small</label>' + '<label style="font-weight:normal;margin-bottom:0;cursor:pointer;white-space: normal;display:inline" id="jsplus_bootstrap_row_add_downcolTypemedium_label_%id%"><input data-col="medium" style="margin-left:15px;" type="radio" ' + (aw["col-type"] == "medium" ? "checked" : "") + ' name="jsplus_bootstrap_row_add_down_radio_%id%">&nbsp;Medium</label>' + '<label style="font-weight:normal;margin-bottom:0;cursor:pointer;white-space: normal;display:inline" id="jsplus_bootstrap_row_add_downcolTypelarge_label_%id%"><input data-col="large" style="margin-left:15px;" type="radio" ' + (aw["col-type"] == "large" ? "checked" : "") + ' name="jsplus_bootstrap_row_add_down_radio_%id%">&nbsp;Large</label>' + "</td></tr></tbody></table>";
                        l = [['<div style="width:36px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:12px;margin:0 auto;line-height:35px;height:36px;">1</div></div>', '<div class="small-1 columns">col-1</div>', "html"], ['<div style="width:36px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:14px;margin:0 auto;line-height:35px;height:36px;">2</div></div>', '<div class="small-2 columns">col-2</div>', "html"], ['<div style="width:36px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:16px;margin:0 auto;line-height:35px;height:36px;">3</div></div>', '<div class="small-3 columns">col-3</div>', "html"], ['<div style="width:36px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:18px;margin:0 auto;line-height:35px;height:36px;">4</div></div>', '<div class="small-4 columns">col-4</div>', "html"], ['<div style="width:36px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:20px;margin:0 auto;line-height:35px;height:36px;">5</div></div>', '<div class="small-5 columns">col-5</div>', "html"], ['<div style="width:36px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:22px;margin:0 auto;line-height:35px;height:36px;">6</div></div>', '<div class="small-6 columns">col-6</div>', "html"], ['<div style="width:36px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:24px;margin:0 auto;line-height:35px;height:36px;">7</div></div>', '<div class="small-7 columns">col-7</div>', "html"], ['<div style="width:36px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:26px;margin:0 auto;line-height:35px;height:36px;">8</div></div>', '<div class="small-8 columns">col-8</div>', "html"], ['<div style="width:35px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:27px;margin:0 auto;line-height:35px;height:36px;">9</div></div>', '<div class="small-9 columns">col-9</div>', "html"], ['<div style="width:35px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:29px;margin:0 auto;line-height:35px;height:36px;">10</div></div>', '<div class="small-10 columns">col-10</div>', "html"], ['<div style="width:34px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:30px;margin:0 auto;line-height:35px;height:36px;">11</div></div>', '<div class="small-11 columns">col-11</div>', "html"], ['<div style="width:34px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#008cba;border:#007095 1px solid;text-align:center;font-size:12px;color:white;width:32px;margin:0 auto;line-height:35px;height:36px;">12</div></div>', '<div class="small-12 columns">col-12</div>', "html"], ['<div style="width:100px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:36px;"> 1col 900<br/>extra wide </div></div>', '<div class="row"><div class="col-md-12">&nbsp;</div></div>', "html"], ['<div style="width:100px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:36px;"> 1col 820<br/>wide </div></div>', '<div class="row columns-12 width-820"><div class="col-md-12">&nbsp;</div></div>', "html"], ['<div style="width:100px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:36px;"> 2col 700<br/>(332x2 36) </div></div>', '<div class="row width-700 columns-6 padding-18"><div class="col-md-6 first">&nbsp;</div><div class="col-md-6 last">&nbsp;</div></div>', "html"], ['<div style="width:100px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:36px;"> 3col 900<br/>(260x3 60) </div></div>', '<div class="row columns-3 padding-30"><div class="col-xs-4 first">&nbsp;</div><div class="col-xs-4">&nbsp;</div><div class="col-xs-4 last">&nbsp;</div></div>', "html"],  ['<div style="width:100px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:36px;"> 1col 700<br/>default </div></div>', '<div class="row columns-12 width-700"><div class="col-md-12">&nbsp;</div></div>', "html"], ['<div style="width:100px;height:36px;margin-bottom:8px;margin-top:8px"><div style="background-color:#9174C0;border:#6f5499 1px solid;text-align:center;font-size:12px;color:white;width:90px;margin:0 auto;line-height:18px;height:36px;"> 1col 410<br/>narrow </div></div>', '<div class="row columns-12 width-410"><div class="col-md-12">&nbsp;</div></div>', "html"]];
                    } else {
                        if (q == 10) {
                            y("use_dialog", false);
                            aJ = 345;
                            aL = 370;
                            aF = 360;
                            l = [['<div style="min-width:350px;padding: 7px;box-sizing: border-box;">' + '<div style="padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #3c759d;background-color: #dff0d8;border-color: #d6e9c6;font-size:12px">' + '<strong style="cursor:pointer;color:#3c759d;font-size:12px">Success!</strong> Your request was finished successfully.' + "</div>" + "</div>", '<div style="margin:10px 0; padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #3c763d;background-color: #dff0d8;border-color: #d6e9c6;">' + "<strong>Success!</strong> Your request was finished successfully." + "</div>", "html"], ['<div style="min-width:350px;padding: 7px;box-sizing: border-box;">' + '<div style="padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #31708f;background-color: #d9edf7;border-color: #bce8f1;font-size:12px">' + '<strong style="cursor:pointer;color:#31708f;font-size:12px">Info.</strong> Here are details for your query.' + "</div>" + "</div>", '<div style="margin:10px 0; padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #31708f;background-color: #d9edf7;border-color: #bce8f1;">' + "<strong>Info.</strong> Here are details for your query." + "</div>", "html"], ['<div style="min-width:350px;padding: 7px;box-sizing: border-box;">' + '<div style="padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #8a6d3b;background-color: #fcf8e3;border-color: #faebcc;font-size:12px">' + '<strong style="cursor:pointer;color:#8a6d3b;font-size:12px">Warning!</strong> Are you sure you want to proceed?' + "</div>" + "</div>", '<div style="margin:10px 0; padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #8a6d3b;background-color: #fcf8e3;border-color: #faebcc;">' + "<strong>Warning!</strong> Are you sure you want to proceed?" + "</div>", "html"], ['<div style="min-width:350px;padding: 7px;box-sizing: border-box;">' + '<div style="padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #a94442;background-color: #f2dede;border-color: #ebccd1;font-size:12px">' + '<strong style="cursor:pointer;color:#a94442;font-size:12px">Danger!</strong> System state is critical, check the configuration!' + "</div>" + "</div>", '<div style="margin:10px 0; padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #a94442;background-color: #f2dede;border-color: #ebccd1;">' + "<strong>Danger!</strong> System state is critical, check the configuration!" + "</div>", "html"], ['<div style="min-width:350px;padding: 7px;box-sizing: border-box;">' + '<div style="padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #777;background-color: #eee;border-color: #ddd;font-size:12px">' + '<strong style="cursor:pointer;color:#777;font-size:12px">Message.</strong> Lorem ipsum dolor sit amet, consectetur.' + "</div>" + "</div>", '<div style="margin:10px 0; padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #777;background-color: #eee;border-color: #ddd;">' + "<strong>Message.</strong> Lorem ipsum dolor sit amet, consectetur." + "</div>", "html"]];
                        } else {
                            if (q == 11) {
                                y("use_dialog", false);
                                aJ = 400;
                                aL = 420;
                                aF = 300;
                                l = [['<div style="width:385px;padding: 7px;box-sizing: border-box;"><div style="margin-bottom:0;padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #3c763d;background-color: #dff0d8;border-color: #d6e9c6;font-size:12px"><strong style="cursor:pointer;color:#3c763d;font-size:12px">Success!</strong> Your request was finished successfully</div></div>', '<div class="alert alert-success" role="alert"><strong>Success!</strong>Your request was finished successfully</div>', "html"], ['<div style="width:385px;padding: 7px;box-sizing: border-box;"><div style="margin-bottom:0;padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #31708f;background-color: #d9edf7;border-color: #bce8f1;font-size:12px"><strong style="cursor:pointer;color: #31708f;font-size:12px">Info.</strong> Here are details for your query.</div></div>', '<div class="alert alert-info" role="alert"><strong>Info.</strong> Here are details for your query.</div>', "html"], ['<div style="width:385px;padding: 7px;box-sizing: border-box;"><div style="margin-bottom:0;padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #8a6d3b;background-color: #fcf8e3;border-color: #faebcc;font-size:12px"><strong style="cursor:pointer;color:#8a6d3b;font-size:12px">Warning!</strong> Are you sure you want to proceed?</div></div>', '<div class="alert alert-warning" role="alert"><strong>Warning!</strong> Are you sure you want to proceed?</div>', "html"], ['<div style="width:385px;padding: 7px;box-sizing: border-box;"><div style="margin-bottom:0;padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #a94442;background-color: #f2dede;border-color: #ebccd1;font-size:12px"><strong style="cursor:pointer;color:#a94442;font-size:12px">Danger!</strong> System state is critical, check the configuration!</div></div>', '<div class="alert alert-danger" role="alert"><strong>Danger!</strong> System state is critical, check the configuration!</div>', "html"]];
                            } else {
                                if (q == 12) {
                                    aJ = 345;
                                    aL = 370;
                                    aF = 300;
                                    l = [['<div style="min-width:350px;padding: 7px;box-sizing: border-box;"><div style="margin-bottom:0;padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #3c763d;background-color: #dff0d8;border-color: #d6e9c6;font-size:12px"><strong style="cursor:pointer;color:#3c763d;font-size:12px">Success!</strong> Your request was finished successfully</div></div>', '<div class="alert-box success" role="alert"><strong>Success!</strong>Your request was finished successfully</div>', "html"], ['<div style="min-width:350px;padding: 7px;box-sizing: border-box;"><div style="margin-bottom:0;padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #31708f;background-color: #d9edf7;border-color: #bce8f1;font-size:12px"><strong style="cursor:pointer;color: #31708f;font-size:12px">Info.</strong> Here are details for your query.</div></div>', '<div class="alert-box info" role="alert"><strong>Info.</strong> Here are details for your query.</div>', "html"], ['<div style="min-width:350px;padding: 7px;box-sizing: border-box;"><div style="margin-bottom:0;padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #8a6d3b;background-color: #fcf8e3;border-color: #faebcc;font-size:12px"><strong style="cursor:pointer;color:#8a6d3b;font-size:12px">Warning!</strong> Are you sure you want to proceed?</div></div>', '<div class="alert-box warning" role="alert"><strong>Warning!</strong> Are you sure you want to proceed?</div>', "html"], ['<div style="min-width:350px;padding: 7px;box-sizing: border-box;"><div style="margin-bottom:0;padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #a94442;background-color: #f2dede;border-color: #ebccd1;font-size:12px"><strong style="cursor:pointer;color:#a94442;font-size:12px">Danger!</strong> System state is critical, check the configuration!</div></div>', '<div class="alert-box alert" role="alert"><strong>Danger!</strong> System state is critical, check the configuration!</div>', "html"], ['<div style="min-width:350px;padding: 7px;box-sizing: border-box;"><div style="margin-bottom:0;padding: 15px;border: 1px solid transparent;border-radius: 4px;color: #4f4f4f;background-color: #e7e7e7;border-color: #c7c7c7;font-size:12px"><strong style="cursor:pointer;color:#979797;font-size:12px">Message.</strong>  Lorem ipsum dolor sit amet, consectetur.</div></div>', '<div class="alert-box secondary" role="alert"><strong>Message.</strong> This is the simple message</div>', "html"]];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    function g(aX) {
        var aW = "-" + H(aX);
        var aV = document.getElementById("jsplus_bootstrap_row_add_down-dialog-root" + aW);
        if (aV != null && aV.children.length == 0) {
            aV.innerHTML = "";
        }
        aV = m.getElementById("jsplus_bootstrap_row_add_down-popup-root" + aW);
        aV.innerHTML = D.replace(/%id%/g, H(aX)).replace(/\{\{([^}]*)\}\}/g, function (aZ, aY) {
            return z(aX, aY);
        });
        h(aX, aV.ownerDocument);
    }

    function E(aY) {
        var aW = document.getElementsByClassName("jsplus_bootstrap_row_add_down-selected");
        var aX = null;
        for (var aV = 0; aV < aW.length && aX == null; aV++) {
            if (aU(aW[aV], "jsplus_bootstrap_row_add_down-template-" + H(aY))) {
                aX = aW[aV];
            }
        }
        if (aX == null) {
            alert(z(aY, "select_element_first"));
            window["jsplus_bootstrap_row_add_down_callback"] = null;
            return false;
        } else {
            R(aY, aX.getAttribute("data-jsplus_bootstrap_row_add_down_template_id"));
            window["jsplus_bootstrap_row_add_down_callback"] = null;
        }
    }

    function Q(aW) {
        var aV;
        if (m != null) {
            aV = m.getElementById("jsplus_bootstrap_row_add_down-popup-root-" + H(aW));
            if (aV != null && aV.children.length == 0) {
                aV.innerHTMLAddition = null;
            }
        }
        aV = document.getElementById("jsplus_bootstrap_row_add_down-dialog-root-" + H(aW));
        if (aV.children.length == 0) {
            aV.innerHTML = aV.innerHTML = D.replace(/%id%/g, H(aW)).replace(/\{\{([^}]*)\}\}/g, function (aY, aX) {
                return z(aW, aX);
            });
        }
        h(aW, aV.ownerDocument);
        ag(aW, -1);
    }

    function aO(aV) {
        window["jsplus_bootstrap_row_add_down_callback"] = null;
    }

    function O(aV) {
        return window["jsplus_bootstrap_row_add_down_callback"] != null || af(aV, "use_dialog");
    }

    function h(aZ, a1) {
        if (q == 1 || q == 101 || q == 201) {
            var aY = function () {
                aw["col-type"] = this.getAttribute("data-col");
            };
            a1.getElementById("jsplus_bootstrap_row_add_downcolTypeXS_label_" + H(aZ)).getElementsByTagName("input")[0].onchange = aY;
            a1.getElementById("jsplus_bootstrap_row_add_downcolTypeSM_label_" + H(aZ)).getElementsByTagName("input")[0].onchange = aY;
            a1.getElementById("jsplus_bootstrap_row_add_downcolTypeMD_label_" + H(aZ)).getElementsByTagName("input")[0].onchange = aY;
            a1.getElementById("jsplus_bootstrap_row_add_downcolTypeLG_label_" + H(aZ)).getElementsByTagName("input")[0].onchange = aY;
            a1.getElementById("jsplus_bootstrap_row_add_downcolTypeXL_label_" + H(aZ)).getElementsByTagName("input")[0].onchange = aY;
            if (window.jsplus_bootstrap_version && window.jsplus_bootstrap_version == 3) {
                F(a1.getElementById("jsplus_bootstrap_row_add_down_div_xl_" + H(aZ), "display", "none"));
            }
        } else {
            if (q == 2 || q == 102 || q == 202) {
                var aY = function () {
                    aw["col-type"] = this.getAttribute("data-col");
                };
                a1.getElementById("jsplus_bootstrap_row_add_downcolTypesmall_label_" + H(aZ)).getElementsByTagName("input")[0].onchange = aY;
                a1.getElementById("jsplus_bootstrap_row_add_downcolTypemedium_label_" + H(aZ)).getElementsByTagName("input")[0].onchange = aY;
                a1.getElementById("jsplus_bootstrap_row_add_downcolTypelarge_label_" + H(aZ)).getElementsByTagName("input")[0].onchange = aY;
            } else {
                if (q == 20 || q == 21 || q == 22) {
                    a1.getElementById("jsplus_bootstrap_row_add_down_add_header_" + H(aZ)).checked = af(aZ, "default_add_header");
                    a1.getElementById("jsplus_bootstrap_row_add_down_add_header_" + H(aZ)).onchange = function () {
                        aw["add_header"] = this.checked;
                    };
                    if (q == 20) {
                        a1.getElementById("jsplus_bootstrap_row_add_down_width_100_" + H(aZ)).checked = af(aZ, "default_width_100");
                        a1.getElementById("jsplus_bootstrap_row_add_down_width_100_" + H(aZ)).onchange = function () {
                            aw["width_100"] = this.checked;
                        };
                    }
                    if (q == 21) {
                        a1.getElementById("jsplus_bootstrap_row_add_down_striped_" + H(aZ)).checked = af(aZ, "default_striped");
                        a1.getElementById("jsplus_bootstrap_row_add_down_striped_" + H(aZ)).onchange = function () {
                            aw["striped"] = this.checked;
                        };
                        a1.getElementById("jsplus_bootstrap_row_add_down_bordered_" + H(aZ)).checked = af(aZ, "default_bordered");
                        a1.getElementById("jsplus_bootstrap_row_add_down_bordered_" + H(aZ)).onchange = function () {
                            aw["bordered"] = this.checked;
                        };
                        a1.getElementById("jsplus_bootstrap_row_add_down_condensed_" + H(aZ)).checked = af(aZ, "default_condensed");
                        a1.getElementById("jsplus_bootstrap_row_add_down_condensed_" + H(aZ)).onchange = function () {
                            aw["condensed"] = this.checked;
                        };
                    }
                    a1.getElementById("jsplus_bootstrap_row_add_down_class_" + H(aZ)).value = af(aZ, "default_class");
                    a1.getElementById("jsplus_bootstrap_row_add_down_style_" + H(aZ)).value = af(aZ, "default_style");
                    a1.getElementById("jsplus_bootstrap_row_add_down_options_" + H(aZ)).style.display = af(aZ, "show_options") ? "block" : "none";
                    a1.getElementById("jsplus_bootstrap_row_add_down_advanced_options_" + H(aZ)).style.display = af(aZ, "show_advanced_options") ? "block" : "none";
                    var a0 = a1.getElementById("jsplus_bootstrap_row_add_down_class_" + H(aZ));
                    a0.onchange = function () {
                        aw["class"] = this.value;
                    };
                    a0.oninput = a0.onchange;
                    a0.onpase = a0.onchange;
                    a0.onkeyup = a0.onchange;
                    var aX = a1.getElementById("jsplus_bootstrap_row_add_down_style_" + H(aZ));
                    aX.onchange = function () {
                        aw["style"] = this.value;
                    };
                    aX.oninput = aX.onchange;
                    aX.onpase = aX.onchange;
                    aX.onkeyup = aX.onchange;
                }
            }
        }
        var aV = a1.getElementsByClassName("jsplus_bootstrap_row_add_down-template-" + H(aZ));
        var a2 = function () {
            var a3 = this.getAttribute("data-jsplus_bootstrap_row_add_down_template_id");
            O(aZ) ? ag(aZ, a3) : x(aZ, a3);
        };
        for (var aW = 0; aW < aV.length; aW++) {
            aV[aW].onclick = a2;
            if (q == 20 || q == 21 || q == 22) {
                aV[aW].onmouseover = (function () {
                    var a3 = a1;
                    return function () {
                        var a7 = this.getAttribute("data-jsplus_bootstrap_row_add_down_template_id");
                        var a4 = a7 % 6;
                        var a8 = Math.floor(a7 / 6);
                        for (var a9 = 0; a9 < 6; a9++) {
                            for (var a5 = 0; a5 < 6; a5++) {
                                var a6 = a3.getElementById("jsplus_bootstrap_row_add_down_cell_" + a9 + "_" + a5 + "_" + H(aZ));
                                a6.style.opacity = (a9 <= a4 && a5 <= a8) ? "1" : "0.45";
                            }
                        }
                    };
                })();
            }
        }
    }

    function X(aW) {
        K(aW, "mode", r);
        K(aW, "instanceReady", r);
        K(aW, "selectionChange", r);
        if (q == 20 || q == 21 || q == 22) {
            if (q == 20) {
                aw["width_100"] = af(aW, "default_width_100");
            }
            aw["add_header"] = af(aW, "default_add_header");
            if (q == 21) {
                aw["striped"] = af(aW, "default_striped");
                aw["bordered"] = af(aW, "default_bordered");
                aw["condensed"] = af(aW, "default_condensed");
            }
            tableWidth = 154;
            optionsWidth = 0;
            if (af(aW, "show_options") === true) {
                optionsWidth += 100;
                tableWidth += 15;
                if (af(aW, "show_advanced_options") === true) {
                    optionsWidth += 100;
                }
                tableWidth += optionsWidth;
            }
            aJ = tableWidth + 20;
            aL = tableWidth + 40;
            aF = 250;
        }
        if (q == 3) {
            l = ab(aW, "jsplus_bootstrap_row_add_down");
            aJ = af(aW, "popup_width");
            aL = af(aW, "dialog_width");
            aF = af(aW, "dialog_height");
        } else {
            if (q == 0) {
            } else {
                if (q == 1 || q == 101 || q == 201) {
                    if (["xs", "sm", "md", "lg", "xl"].indexOf(af(aW, "default_col_type").toLowerCase()) != -1) {
                        aw["col-type"] = "md";
                    }
                    G = G.replace('jsplus_bootstrap_row_add_down_radio_%id%"/>&nbsp;' + aw["col-type"].toUpperCase(), 'jsplus_bootstrap_row_add_down_radio_%id%" checked/>&nbsp;' + aw["col-type"].toUpperCase());
                } else {
                    if (q == 2 || q == 102 || q == 202) {
                        if (["small", "medium", "large"].indexOf(af(aW, "default_col_type").toLowerCase()) != -1) {
                            aw["col-type"] = "medium";
                        }
                    } else {
                        if (q == 10) {
                        } else {
                            if (q == 11) {
                            } else {
                                if (q == 12) {
                                }
                            }
                        }
                    }
                }
            }
        }
        D = "";
        for (var aV = 12; aV < l.length; aV++) {
            D += '<div data-jsplus_bootstrap_row_add_down_template_id="' + aV + '" class="jsplus_bootstrap_row_add_down-template jsplus_bootstrap_row_add_down-template-%id%">' + l[aV][0].replace(/\{path\}/g, aM(aW)) + "</div>";
        }
        if (q == 20 || q == 21 || q == 22) {
            I = I.replace(/%table_width%/g, tableWidth);
            G = G.replace(/%options_width%/g, optionsWidth);
        }
        D = '<style type="text/css">' + ".jsplus_bootstrap_row_add_down-template { display: inline-block; }" + ".jsplus_bootstrap_row_add_down-template, .jsplus_bootstrap_row_add_down-template div { cursor:pointer }" + ((q != 20 && q != 21 && q != 22) ? ".jsplus_bootstrap_row_add_down-template:hover > div, .jsplus_bootstrap_row_add_down-template.jsplus_bootstrap_row_add_down-selected > div{ outline: 1px silver solid; background: #EFEFFF;}" : "") + "</style>" + '<div class="jsplus_bootstrap_row_add_down-panel-%id%" style="white-space: normal">' + D + "</div>";
    }

    function x(aV, aW) {
        R(aV, aW);
        at(aV);
    }

    function ag(aX, aY) {
        var aW = document.getElementsByClassName("jsplus_bootstrap_row_add_down-template-" + H(aX));
        for (var aV = 0; aV < aW.length; aV++) {
            aW[aV].className = aW[aV].className.replace(/\s*jsplus_bootstrap_row_add_down-selected/, "");
            if (parseInt(aW[aV].getAttribute("data-jsplus_bootstrap_row_add_down_template_id")) == aY) {
                aW[aV].className += " jsplus_bootstrap_row_add_down-selected";
            }
        }
    }

    function R(a4, aZ) {
        var a8 = null;
        var a1 = false;
        if (typeof(window["jsplus_bootstrap_row_add_down_callback"]) != "undefined" && window["jsplus_bootstrap_row_add_down_callback"] != null) {
            a8 = window["jsplus_bootstrap_row_add_down_callback"]["func"];
            a1 = window["jsplus_bootstrap_row_add_down_callback"]["reverseOrder"];
            window["jsplus_bootstrap_row_add_down_callback"] = null;
        }
        var a7 = "html";
        var a2 = l[aZ][0].slice(0);
        if (l[aZ].length > 1) {
            a2 = l[aZ][1].slice(0);
            if (l[aZ].length > 2) {
                a7 = l[aZ][2];
            }
        }
        if (Object.prototype.toString.call(a2) === "[object Array]") {
            for (var a0 = 0; a0 < a2.length; a0++) {
                a2[a0] = a2[a0].replace(/\{path\}/g, aM());
            }
        } else {
            a2 = a2.replace(/\{path\}/g, aM());
        }
        if (a7 == "table" && (q == 20 || q == 21 || q == 22)) {
            var a3 = a2.split(",");
            var aX = a3[0];
            var bb = a3[1];
            var ba = [];
            if (q == 21) {
                ba.push("table");
                if (aw["striped"]) {
                    ba.push("table-striped");
                }
                if (aw["bordered"]) {
                    ba.push("table-bordered");
                }
                if (aw["condensed"]) {
                    ba.push("table-condensed");
                }
            }
            if (aw["class"].trim().length > 0) {
                ba.push(aw["class"].trim());
            }
            if (ax() == "ckeditor") {
                ba.push("cke_show_border");
            }
            var a9 = aw["style"].trim();
            if (q == 20 && aw["width_100"]) {
                if (a9.length > 0 && a9.substr(a9.length - 1, 1) != ";") {
                    a9 += "; ";
                }
                a9 += "width: 100%";
            }
            a2 = "<table" + (ba.length > 0 ? (" class='" + ba.join(" ") + "'") : "") + (a9.length > 0 ? (" style='" + a9 + "'") : "") + ">\n";
            if (aw["add_header"]) {
                a2 += "\t<thead>\n\t\t<tr>\n";
                for (var a0 = 0; a0 <= aX; a0++) {
                    a2 += "\t\t\t<th>&nbsp;</th>\n";
                }
                a2 += "\t\t</tr>\n\t</thead>\n";
            }
            a2 += "\t<tbody>\n";
            for (var aV = 0; aV <= bb; aV++) {
                a2 += "\t\t<tr>\n";
                for (var a6 = 0; a6 <= aX; a6++) {
                    a2 += "\t\t\t<td>&nbsp;</td>\n";
                }
                a2 += "\t\t</tr>\n";
            }
            a2 += "\t</tbody>\n";
            a2 += "</table>\n";
            aI(a4, a2);
        } else {
            if (a7 == "text") {
                aI(a4, "<span>" + a2 + "</span>");
            } else {
                if (a7 == "html") {
                    if (q == 1 || q == 101 || q == 201) {
                        if (aw["col-type"] == "sm") {
                            a2 = a2.replace(/col-xs/g, "col-sm");
                        } else {
                            if (aw["col-type"] == "md") {
                                a2 = a2.replace(/col-xs/g, "col-md");
                            } else {
                                if (aw["col-type"] == "lg") {
                                    a2 = a2.replace(/col-xs/g, "col-lg");
                                } else {
                                    if (aw["col-type"] == "xl") {
                                        a2 = a2.replace(/col-xs/g, "col-xl");
                                    }
                                }
                            }
                        }
                    } else {
                        if (q == 2 || q == 102 || q == 202) {
                            if (aw["col-type"] != "small") {
                                a2 = a2.replace(/small/g, aw["col-type"]);
                            }
                        }
                    }
                    if (q != 101 && q != 102 && q != 201 && q != 202) {
                        aD(a4, a2, a8);
                    } else {
                        var aW = f(a4);
                        var a5 = av(a2)[0];
                        if (q == 101 || q == 102) {
                            aW.parentNode.insertBefore(a5, aW);
                        } else {
                            var aY = aW.nextSibling;
                            if (aY != null) {
                                aW.parentNode.insertBefore(a5, aY);
                            } else {
                                aW.parentNode.appendChild(a5);
                            }
                        }
                        if (ax() == "ckeditor") {
                            a4.focus();
                        }
                    }
                }
            }
        }
    }

    function aD(aW, aV, aX) {
        if (typeof(aX) == "undefined" || aX == null) {
            aI(aW, aV);
        } else {
            callFunction(aW, aX, aV);
        }
    }

    function f(aX) {
        var aW = ai(aX);
        var aV = false;
        while (!aV && aW != null) {
            aV = b(aW);
            if (!aV) {
                aW = aW.parentNode;
            }
        }
        return aW;
    }

    function b(aV) {
        return aV != null && aV.tagName == "DIV" && (aU(aV, "flexbox") || aU(aV, "single-col"));
    }

    function p(aV) {
        t(aV, "jsplus_bootstrap_row_add_down", aa);
    }

    function r(aV) {
        if (q == 101 || q == 102 || q == 201 || q == 202) {
            t(aV, "jsplus_bootstrap_row_add_down", (f(aV) == null ? aa : M));
        }
    }

    function av(aV) {
        var aW = document.createElement("div");
        aW.innerHTML = aV;
        return aW.childNodes;
    }

    function aE(aV) {
        return aV.getElementsByTagName("head")[0];
    }

    function aB(aV) {
        return aV.getElementsByTagName("body")[0];
    }

    function aP(aX, aZ) {
        var aV = aX.getElementsByTagName("link");
        var aY = false;
        for (var aW = aV.length - 1; aW >= 0; aW--) {
            if (aV[aW].href == aZ) {
                aV[aW].parentNode.removeChild(aV[aW]);
            }
        }
    }

    function am(aY, a0) {
        if (!aY) {
            return;
        }
        var aV = aY.getElementsByTagName("link");
        var aZ = false;
        for (var aW = 0; aW < aV.length; aW++) {
            if (aV[aW].href.indexOf(a0) != -1) {
                aZ = true;
            }
        }
        if (!aZ) {
            var aX = aY.createElement("link");
            aX.href = a0;
            aX.type = "text/css";
            aX.rel = "stylesheet";
            aE(aY).appendChild(aX);
        }
    }

    function o(aY, a0) {
        if (!aY) {
            return;
        }
        var aV = aY.getElementsByTagName("script");
        var aZ = false;
        for (var aX = 0; aX < aV.length; aX++) {
            if (aV[aX].src.indexOf(a0) != -1) {
                aZ = true;
            }
        }
        if (!aZ) {
            var aW = aY.createElement("script");
            aW.src = a0;
            aW.type = "text/javascript";
            aE(aY).appendChild(aW);
        }
    }

    function aR(aV, aX, aW) {
        am(c(aV), aX);
        if (document != c(aV) && aW) {
            am(document, aX);
        }
    }

    function aq(aV, aX, aW) {
        o(c(aV), aX);
        if (document != c(aV) && aW) {
            o(document, aX);
        }
    }

    function aA(aW, aV) {
        var aX = c(aW);
        C(aX, aV);
    }

    function C(aX, aV) {
        var aW = aX.createElement("style");
        aE(aX).appendChild(aW);
        aW.innerHTML = aV;
    }

    function aH(aW, aV) {
        if (aU(aW, aV)) {
            return;
        }
        aW.className = aW.className.length == 0 ? aV : aW.className + " " + aV;
    }

    function aN(aX, aV) {
        var aW = a(aX);
        while (aW.indexOf(aV) > -1) {
            aW.splice(aW.indexOf(aV), 1);
        }
        var aY = aW.join(" ").trim();
        if (aY.length > 0) {
            aX.className = aY;
        } else {
            if (aX.hasAttribute("class")) {
                aX.removeAttribute("class");
            }
        }
    }

    function a(aV) {
        if (typeof(aV.className) === "undefined" || aV.className == null) {
            return [];
        }
        return aV.className.split(/\s+/);
    }

    function aU(aY, aV) {
        var aX = a(aY);
        for (var aW = 0; aW < aX.length; aW++) {
            if (aX[aW].toLowerCase() == aV.toLowerCase()) {
                return true;
            }
        }
        return false;
    }

    function aS(aX, aY) {
        var aW = a(aX);
        for (var aV = 0; aV < aW.length; aV++) {
            if (aW[aV].indexOf(aY) === 0) {
                return true;
            }
        }
        return false;
    }

    function ak(aX) {
        if (typeof(aX.getAttribute("style")) === "undefined" || aX.getAttribute("style") == null || aX.getAttribute("style").trim().length == 0) {
            return {};
        }
        var aZ = {};
        var aY = aX.getAttribute("style").split(/;/);
        for (var aW = 0; aW < aY.length; aW++) {
            var a0 = aY[aW].trim();
            var aV = a0.indexOf(":");
            if (aV > -1) {
                aZ[a0.substr(0, aV).trim()] = a0.substr(aV + 1);
            } else {
                aZ[a0] = "";
            }
        }
        return aZ;
    }

    function au(aX, aW) {
        var aY = ak(aX);
        for (var aV in aY) {
            var aZ = aY[aV];
            if (aV == aW) {
                return aZ;
            }
        }
        return null;
    }

    function ao(aY, aX, aV) {
        var aZ = ak(aY);
        for (var aW in aZ) {
            var a0 = aZ[aW];
            if (aW == aX && a0 == aV) {
                return true;
            }
        }
        return false;
    }

    function F(aX, aW, aV) {
        var aY = ak(aX);
        aY[aW] = aV;
        w(aX, aY);
    }

    function al(aW, aV) {
        var aX = ak(aW);
        delete aX[aV];
        w(aW, aX);
    }

    function w(aW, aY) {
        var aX = [];
        for (var aV in aY) {
            aX.push(aV + ":" + aY[aV]);
        }
        if (aX.length > 0) {
            aW.setAttribute("style", aX.join(";"));
        } else {
            if (aW.hasAttribute("style")) {
                aW.removeAttribute("style");
            }
        }
    }

    function B(aZ, aW) {
        var aX;
        if (Object.prototype.toString.call(aW) === "[object Array]") {
            aX = aW;
        } else {
            aX = [aW];
        }
        for (var aY = 0; aY < aX.length; aY++) {
            aX[aY] = aX[aY].toLowerCase();
        }
        var aV = [];
        for (var aY = 0; aY < aZ.childNodes.length; aY++) {
            if (aZ.childNodes[aY].nodeType == 1 && aX.indexOf(aZ.childNodes[aY].tagName.toLowerCase()) > -1) {
                aV.push(aZ.childNodes[aY]);
            }
        }
        return aV;
    }

    function aC(aW) {
        var a0 = new RegExp("(^|.*[\\/])" + aW + ".js(?:\\?.*|;.*)?$", "i");
        var aZ = "";
        if (!aZ) {
            var aV = document.getElementsByTagName("script");
            for (var aY = 0; aY < aV.length; aY++) {
                var aX = a0.exec(aV[aY].src);
                if (aX) {
                    aZ = aX[1];
                    break;
                }
            }
        }
        if (aZ.indexOf(":/") == -1 && aZ.slice(0, 2) != "//") {
            if (aZ.indexOf("/") === 0) {
                aZ = location.href.match(/^.*?:\/\/[^\/]*/)[0] + aZ;
            } else {
                aZ = location.href.match(/^[^\?]*\/(?:)/)[0] + aZ;
            }
        }
        return aZ.length > 0 ? aZ : null;
    }

    function P(aV, aZ, aX) {
        if (typeof aZ == "undefined") {
            aZ = true;
        }
        if (typeof aX == "undefined") {
            aX = " ";
        }
        if (typeof(aV) == "undefined") {
            return "";
        }
        var a0 = 1000;
        if (aV < a0) {
            return aV + aX + (aZ ? "b" : "");
        }
        var aW = ["K", "M", "G", "T", "P", "E", "Z", "Y"];
        var aY = -1;
        do {
            aV /= a0;
            ++aY;
        } while (aV >= a0);
        return aV.toFixed(1) + aX + aW[aY] + (aZ ? "b" : "");
    }

    function ap(aV) {
        return aV.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
    }

    function aG(aV) {
        return aV.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function at(aW) {
        var aV = aj(aW);
        if (aV) {
            aV.close();
        }
    }

    tinymce.PluginManager.requireLangPack("jsplus_bootstrap_row_add_down");
    tinymce.PluginManager.add("jsplus_bootstrap_row_add_down", function (aX, aW) {
        var aV = function (a0) {
            X(a0);
            D = I + D + G;
            if (O(a0)) {
                return '<div id="jsplus_bootstrap_row_add_down-dialog-root-' + H(a0) + '" style="padding:8px">' + D.replace(/%id%/g, H(a0)).replace(/\{\{([^}]*)\}\}/g, function (a2, a1) {
                    return z(a0, a1);
                }) + "</div>";
            } else {
                return '<div id="jsplus_bootstrap_row_add_down-popup-root-' + H(a0) + '"' + (af(a0, "popup_width") != null ? (' style="width:' + af(a0, "popup_width") + 'px"') : "") + ">" + D.replace(/%id%/g, H(a0)).replace(/\{\{([^}]*)\}\}/g, function (a2, a1) {
                    return z(a0, a1);
                }) + "</div>";
            }
        };
        var aZ = function (a2) {
            if (O(a2)) {
                var a1 = aV(a2);
                var a3 = Y(a2, "jsplus_bootstrap_row_add_down", z(a2, "jsplus_bootstrap_row_add_down_title"), aL, a1, [{
                    title: z(a2, "btn_ok"),
                    type: "ok"
                }, {title: z(a2, "btn_cancel"), type: "cancel"},], function (a4) {
                    return E(a4);
                }, function (a4) {
                    aO(a4);
                }, function (a4) {
                    Q(a4);
                }, function (a4) {
                }, function (a4) {
                });
                a3.open(a2);
            } else {
                var a0 = aT(a2, aJ, aV(a2).replace(/%id%/g, H(a2)), function (a4) {
                    m = document;
                }, function (a4) {
                }, function (a4) {
                    m = document;
                    g(a4);
                });
                a0.open();
            }
        };
        var aY = "";
        if (q == 1 || q == 101 || q == 201 || q == 11 || q == 21) {
            if (ab(aX, "jsplus_bootstrap_include_bw_icons") === true) {
                aY = "_bw";
            }
        }
        if (q == 2 || q == 102 || q == 202 || q == 12 || q == 22) {
            if (ab(aX, "jsplus_foundation_include_bw_icons") === true) {
                aY = "_bw";
            }
        }
        ae(aX, "jsplus_bootstrap_row_add_down", aM() + "mce_icons/jsplus_bootstrap_row_add_down" + v() + aY + ".png", z(aX, "jsplus_bootstrap_row_add_down_title"), !aX.getParam("jsplus_bootstrap_row_add_down_use_dialog"), aZ);
        if (q == 101 || q == 102 || q == 201 || q == 202) {
            r(aX);
            W(aX, r);
            K(aX, "mode", p);
        }
    });
})();
